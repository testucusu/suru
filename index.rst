Welcome
================================
Welcome to the documentation of suru repository by TEST UCUSU

This repository is created for TEKNOFEST 2020 Swarm UAV Simulation Competition

Repository can be downloaded from https://bitbucket.org/testucusu/suru/src

No copyright, just enjoy!

Navigation Module
================================

.. automodule:: testucusu.navigation
   :members:

Utility Module
================================

.. automodule:: testucusu.util
   :members:

.. toctree::
   :hidden:

   index

Test Ucusu Team
================================
List of our team members

Captain, Lead Developer
################################
*Mustafa Gokce,* mustafagokce009@gmail.com

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Member, Lead Tester
################################
*Merve Muslu,* mervemuslu94@gmail.com

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.