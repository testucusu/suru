import numpy as np
import matplotlib.pyplot as plt

with open("casualties_in_world.txt", "r") as obj:
	data = obj.read()

data = data.replace("'","\"")
data = eval(data)

points = []
for i in data:
	pose = (i["pose"])
	points.append(pose)

first_latitude = []
first_longitude = []
last_latitude = []
last_longitude = []

for k in points:
	x =  k[0]
	y =  k[1]
	first_latitude.append(x)
	first_longitude.append(y)

plt.plot(first_latitude, first_longitude, "ro")
plt.xlabel('x - axis')
plt.ylabel('y - axis')
plt.title("First version of gps values")
plt.show()

def distance(p1, p2):
    return ((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2)**(0.5)

def fuse(points, d):
    ret = []
    d2 = (d * d)**(0.5)
    n = len(points)
    taken = [False] * n

    for i in range(n):

        if not taken[i]:
            count = 1
            point = [points[i][0], points[i][1]]
            taken[i] = True

            for j in range(i+1, n):
                if distance(points[i], points[j]) < d2:
                    point[0] += points[j][0]
                    point[1] += points[j][1]
                    count+=1
                    taken[j] = True

            point[0] /= count
            point[1] /= count
            ret.append((point[0], point[1]))
            latitude = point[0]
            longitude = point[1]
            last_latitude.append(latitude)
            last_longitude.append(longitude)

    return ret

fuse(points,d=2)

plt.plot(last_latitude, last_longitude, "bs")
plt.xlabel('x - axis')
plt.ylabel('y - axis')
plt.title("Fused version of gps values")
plt.show()