from testucusu.util import destination, rotate
import matplotlib.pyplot as plt
import numpy as np
import math
from mpl_toolkits import mplot3d

uav_number = 9
u_k = 25.0
a_k = 0.0
u_b = 25.0
uav_list = []

guide_uav_location = (5.0, 5.0, 0.0)
head_uav0_location = destination(guide_uav_location, 180.0 + a_k, u_k)
head_uav0_location = (head_uav0_location[0], head_uav0_location[1], 0)
prism_front_center = destination(head_uav0_location, 180.0, u_b)
prism_front_center = (prism_front_center[0], prism_front_center[1], 0)

uav1 = (prism_front_center[0] - (u_b/2), prism_front_center[1] + (u_b/2), prism_front_center[2])
uav2 = (prism_front_center[0] - (u_b/2), prism_front_center[1] - (u_b/2), prism_front_center[2])
uav3 = (prism_front_center[0] + (u_b/2), prism_front_center[1] + (u_b/2), prism_front_center[2])
uav4 = (prism_front_center[0] + (u_b/2), prism_front_center[1] - (u_b/2), prism_front_center[2])

uav5 = (prism_front_center[0] - (u_b/2), prism_front_center[1] + (u_b/2), prism_front_center[2] + (u_b))
uav6 = (prism_front_center[0] - (u_b/2), prism_front_center[1] - (u_b/2), prism_front_center[2]+ (u_b))
uav7 = (prism_front_center[0] + (u_b/2), prism_front_center[1] + (u_b/2), prism_front_center[2] + (u_b))
uav8 = (prism_front_center[0] + (u_b/2), prism_front_center[1] - (u_b/2), prism_front_center[2]+ (u_b))


print(guide_uav_location)
print(head_uav0_location)
print(prism_front_center)
print(uav1)
print(uav2)
print(uav3)
print(uav4)


ax = plt.axes(projection='3d')

ax.scatter3D([guide_uav_location[0]], [guide_uav_location[1]],[guide_uav_location[2]],c='b', marker='s' )
ax.scatter3D([head_uav0_location[0]], [head_uav0_location[1]], [head_uav0_location[2]], c='b', marker='^')
ax.scatter3D([uav1[0]], [uav1[1], uav1[2]], c="r", marker="o")
ax.scatter3D([uav2[0]], [uav2[1], uav2[2]], c="r", marker="o")
ax.scatter3D([uav3[0]], [uav3[1], uav3[2]], c="r", marker="o")
ax.scatter3D([uav4[0]], [uav4[1], uav4[2]], c="r", marker="o")

ax.scatter3D([uav5[0]], [uav5[1], uav5[2]], c="r", marker="o")
ax.scatter3D([uav6[0]], [uav6[1], uav6[2]], c="r", marker="o")
ax.scatter3D([uav7[0]], [uav7[1], uav7[2]], c="r", marker="o")
ax.scatter3D([uav8[0]], [uav8[1], uav8[2]], c="r", marker="o")




ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')

fig = plt.figure()
plt.show()
