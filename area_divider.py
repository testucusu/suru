import matplotlib.pyplot as plt
import matplotlib.animation as animation
from testucusu.navigation import area_divider, no_fly_zone_creator, avoidance

# configurations
my_uav_number = 9
my_buffer_zone_width = 10
my_interpolation_distance = 40

# search area boundaries
my_area_points = [(500, 500), (500, -500), (-500, -500), (-500, 500)]

# denied zones
denied_zones = [[[250.0, 250.0], [250.0, 500.0], [500.0, 500.0], [500.0, 250.0]]]

# calculate waypoints and responsibility areas
waypoints, divided_polygons = area_divider(uav_number=my_uav_number,
                                           area_points=my_area_points,
                                           interpolation_distance=my_interpolation_distance,
                                           buffer_zone_width=my_buffer_zone_width)

# create no-fly zones
no_fly_zones, no_fly_zone_points = no_fly_zone_creator(no_fly_zone_circles=None,
                                                       no_fly_zone_polygons=denied_zones,
                                                       interpolation_distance=my_interpolation_distance,
                                                       buffer_zone_width=my_buffer_zone_width)

new_waypoints = []
for divided_waypoints in waypoints:
    new_waypoints.append(avoidance(divided_waypoints, no_fly_zones, no_fly_zone_points))
waypoints = new_waypoints

# create plot axes
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

# limit axes
plt.xlim(-100, 2100)
plt.ylim(-100, 1400)

# set background color
ax.set_facecolor("green")


# animation function for visualization
def animate(i):
    # clear axes
    ax.clear()

    # draw area
    draw = plt.Polygon(my_area_points, color="b", closed=True, fill=False, zorder=99)
    plt.gca().add_line(draw)

    # draw sub search area polygons
    for polygon in divided_polygons:
        if type(polygon) == list:
            for sub in polygon:
                draw = plt.Polygon(sub.boundary.coords, color="r", closed=True, fill=False, zorder=99)
                plt.gca().add_line(draw)
        else:
            draw = plt.Polygon(polygon.boundary.coords, color="b", closed=True, fill=False, zorder=99)
            plt.gca().add_line(draw)

    # draw assigned navigation waypoints
    for j in range(len(waypoints)):
        ax.plot([x.x for x in waypoints[j][:i]], [y.y for y in waypoints[j][:i]], 'ro', zorder=1)
        ax.plot([x.x for x in waypoints[j][i - 10:i]], [y.y for y in waypoints[j][i - 10:i]], 'black', zorder=1)


# call animation function
ani = animation.FuncAnimation(fig, animate, interval=50)

# release plot
plt.show()
