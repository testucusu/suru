import matplotlib.pyplot as plt
import matplotlib.animation as animation
from testucusu import navigation

# configurations
interpolation_distance = 40  # distance between way points
buffer_zone_width = 40  # positive tolerance of no-fly zones, depends on how good you control your uav
speedups_permission = True  # if you want to speed up Shapely calculations

# enable or disable speedups
navigation.speedups(permission=speedups_permission)

# search area boundaries
area_points = [(0, 0),
               (280, 1200),
               (500, 700),
               (1300, 1300),
               (1250, 750),
               (2000, 750),
               (2000, 50),
               (1500, 50),
               (1500, 300),
               (1200, 300),
               (1200, 0)]

"""
area_points = [(0, 0),
               (0, 1000),
               (2000, 1000),
               (2000, 0)]
"""

# no-fly zone boundaries
no_fly_zone_circles = [((300, 400), 150),
                       ((700, 750), 150),
                       ((850, 750), 150)]
no_fly_zone_polygons = [((700, 600), (780, 230), (1000, 200), (600, 100)),
                        ((1100, 500), (1400, 500), (1250, 700))]

# create waypoints
waypoints = navigation.waypoint_creator(area_points, interpolation_distance)

# create no-fly zones and no-fly zone waypoints
no_fly_zones, no_fly_zone_points = navigation.no_fly_zone_creator(no_fly_zone_circles, no_fly_zone_polygons,
                                                                  interpolation_distance, buffer_zone_width)


# path, waypoints = navigation.path_creator(shp.Point(0, 250), shp.Point(2000, 600), 40)
waypoints = navigation.avoidance(waypoints, no_fly_zones, no_fly_zone_points)
print(len(waypoints))


# create plot axes
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

# limit axes
plt.xlim(0, 2000)
plt.ylim(0, 2000)

# set background color
ax.set_facecolor("green")

"""
# draw interpolation points
plt.plot([x.x for x in calculated_waypoints], [y.y for y in calculated_waypoints], 'ro', zorder=1)

# draw area
draw = plt.Polygon(area_points, color="b", closed=True, fill=False, zorder=99)
plt.gca().add_line(draw)

# draw no-fly zones
for zone in no_fly_zone_circles:
    draw = plt.Circle(zone[0], zone[1], color="b", fill=False, zorder=99)
    ax.add_artist(draw)
for zone in no_fly_zone_polygons:
    draw = plt.Polygon(zone, color="b", closed=True, fill=False, zorder=99)
    plt.gca().add_line(draw)

# draw no-fly zone point
for point_list in no_fly_zone_points:
    plt.plot([x.x for x in point_list], [y.y for y in point_list], 'bo', zorder=1)

# show the drawings
plt.show()
"""


def animate(i):
    # limit axes

    ax.clear()

    # plt.xlim(0, 2000)
    # plt.ylim(0, 1000)

    # draw area
    draw = plt.Polygon(area_points, color="b", closed=True, fill=False, zorder=99)
    plt.gca().add_line(draw)

    # draw no-fly zones
    for zone in no_fly_zone_circles:
        draw = plt.Circle(zone[0], zone[1], color="b", fill=False, zorder=99)
        ax.add_artist(draw)
    for zone in no_fly_zone_polygons:
        draw = plt.Polygon(zone, color="b", closed=True, fill=False, zorder=99)
        plt.gca().add_line(draw)

    ax.plot([x.x for x in waypoints[:i]], [y.y for y in waypoints[:i]], 'ro', zorder=1)
    ax.plot([x.x for x in waypoints[i - 10:i]], [y.y for y in waypoints[i - 10:i]], 'black', zorder=1)


ani = animation.FuncAnimation(fig, animate, interval=25)
plt.show()
