from testucusu.object import Point2D
from testucusu.util import destination
import matplotlib.pyplot as plt

uav_number = 9
u_k = 25.0
a_k = 45.0
a_b = 30
u_b = 25.0

guide_uav_location = (5.0, 5.0)
head_uav_location = destination(guide_uav_location, 180.0 + a_k, u_k)

uav = []
k = 0
for i in range(uav_number-1): #1st uav is the guide uav

    if i % 2 == 0:
        k = k + 1
        uav.append(destination(head_uav_location, (180.0 + a_k + (a_b)), (k * (u_b))))
        #print("Number of", [i], "UAV's location is : ", iha[-1])

    else:
        uav.append(destination(head_uav_location, (180.0 + a_k - (a_b)), (k * (u_b))))
        #print("Number of", [i], "UAV's location is : ", iha[-1])

plt.plot(guide_uav_location[0], guide_uav_location[1], "ro")
plt.plot(head_uav_location[0], head_uav_location[1], "ro")
plt.plot([x[0] for x in uav], [y[1] for y in uav], "ro")
plt.xlabel('x - axis')
plt.ylabel('y - axis')
plt.show()
