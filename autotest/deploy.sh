source /opt/ros/kinetic/setup.bash
source /opt/suruiha_2020/devel/setup.bash
bash /gzserver_entrypoint.sh
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:/opt/suruiha_2020/src
export GAZEBO_MODEL_PATH=/opt/suruiha_2020/src/suruiha_2019/suruiha_gazebo_model/gazebo_models
export GAZEBO_PLUGIN_PATH=/opt/suruiha_2020/devel/lib/
service rabbitmq-server start
roslaunch suruiha_gazebo demo_world.launch &
sleep 20
python /opt/suruiha_2020/src/suruiha_2019/suruiha_controller/suruiha_sdk/SuruihaContestManager.py 0 0
