import time
import sys
from testucusu.uav import UAV
from testucusu.object import Waypoint
from testucusu.abbreviation import GotoAltitudeClearingType
from testucusu.navigation import area_divider, no_fly_zone_creator, avoidance
import testucusu.configuration as configuration

# get uav number
uav_count = int(sys.argv[1])

# initiate uav list
my_uavs = []

# get all UAVs up and running
for i in range(uav_count):

    # initialize the UAV
    my_uav = UAV(number=i, name="UAV" + str(i), server_address="localhost")

    # wait for safety
    while not my_uav.safety_switch or not my_uav.scenario.received:
        time.sleep(0.0001)

    # update configurations
    configuration.default_search_altitude = my_uav.scenario.sensor_altitude_maximum - 0.5
    configuration.default_goto_altitude = my_uav.scenario.sensor_altitude_maximum - 0.5

    # create avoidance related objects
    no_fly_zones, no_fly_zone_points = no_fly_zone_creator(no_fly_zone_circles=None,
                                                           no_fly_zone_polygons=my_uav.scenario.denied_zones,
                                                           interpolation_distance=configuration.default_interpolation_distance,
                                                           buffer_zone_width=configuration.default_buffer_zone_width)

    # create waypoints
    waypoints, divided_polygons = area_divider(uav_number=my_uav.scenario.uav_count,
                                               area_points=my_uav.scenario.world_boundaries,
                                               interpolation_distance=configuration.default_interpolation_distance,
                                               buffer_zone_width=configuration.default_buffer_zone_width)
    waypoints = waypoints[my_uav.number]

    # avoid from denied zones
    waypoints = avoidance(waypoints=waypoints,
                          no_fly_zones=no_fly_zones,
                          no_fly_zone_points=no_fly_zone_points)

    # create waypoint object list
    target_locations = []
    for waypoint in waypoints:
        target_locations.append(Waypoint(x=waypoint.x,
                                         y=waypoint.y,
                                         z=configuration.default_search_altitude,
                                         goto_altitude_method=GotoAltitudeClearingType.target_altitude))

    # give a command
    my_uav.search_and_rescue(target_locations=target_locations)

    # arm the vehicle
    my_uav.armed = True

    # append it to list
    my_uavs.append(my_uav)

# proof that server communication is non-blocking
while True:
    try:
        for my_uav in my_uavs:
            print("UAV:", my_uav.number,
                  "Carrying:", my_uav.casualty_carrying,
                  "Extracted:", my_uav.casualty_extracted_count,
                  "Injured:", len(my_uav.casualties_injured),
                  "Waypoint:", my_uav.waypoint_next_count,
                  "Total:", my_uav.waypoint_total_count,
                  "Hospital:", len(my_uav.hospitals))
        time.sleep(1)
    except KeyboardInterrupt:
        print("Bye")
