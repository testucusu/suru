import time
from testucusu.uav import UAV
from testucusu.object import Waypoint
from testucusu.abbreviation import GotoAltitudeClearingType
from testucusu.navigation import waypoint_creator

# initialize the UAV
my_uav = UAV(number=0, name="alpha")

# wait for safety
while not my_uav.safety_switch or not my_uav.scenario.received:
    time.sleep(0.0001)

waypoints = waypoint_creator(my_uav.scenario.world_boundaries, 40)
target_locations = []
for waypoint in waypoints:
    target_locations.append(Waypoint(waypoint.x, waypoint.y, 50.0))

# give a command
my_uav.search_and_rescue(target_locations=target_locations)

# arm the vehicle
my_uav.armed = True

# proof that server communication is non-blocking
while True:
    try:
        print(my_uav.waypoint_next_count, my_uav.waypoint_distance_3d, my_uav.x_speed)
        time.sleep(1)
    except KeyboardInterrupt:
        print("Bye")
