# -*- coding: utf-8 -*-


class Unicodes:
    # OSD place holder
    space = " "

    # OSD box shapes
    vertical = "│"
    horizontal = "─"
    top_left = "┌"
    top_right = "┐"
    bottom_left = "└"
    bottom_right = "┘"
    tri_left = "├"
    tri_right = "┤"

    # OSD item shapes
    up_arrow = "▲"
    heart = "♥"
    square = "■"
    circle = "●"

