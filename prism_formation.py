from testucusu.util import destination, rotate
import matplotlib.pyplot as plt
import numpy as np
import math
from mpl_toolkits import mplot3d

uav_number = 9
u_k = 25.0
a_k = 0.0
u_b = 25.0
uav_list = []

head_uav0_location = destination(guide, 180.0 + a_k, u_k)
head_uav0_location = (head_uav0_location[0], head_uav0_location[1], 0)
prism_front_center = destination(head_uav0_location, 180.0, u_b)
prism_front_center = (prism_front_center[0], prism_front_center[1], 0)


katsayi = int(uav_number/4)
print(katsayi)

k = -1
for i in range (uav_number-1):

    if i % 4 == 0:
        k += 1
        uav_list.append((prism_front_center[0] - (u_b/2), prism_front_center[1] + (u_b/2), prism_front_center[2] + (k*(u_b))))
    if i % 4 == 1:
        uav_list.append((prism_front_center[0] - (u_b/2), prism_front_center[1] - (u_b/2), prism_front_center[2]+ (k*(u_b))))
    if i % 4 == 2:
        uav_list.append((prism_front_center[0] + (u_b/2), prism_front_center[1] + (u_b/2), prism_front_center[2]+ (k*(u_b))))
    if i % 4 == 3:
        uav_list.append((prism_front_center[0] + (u_b/2), prism_front_center[1] - (u_b/2), prism_front_center[2]+ (k*(u_b))))

print(uav_list)


ax = plt.axes(projection='3d')
ax.scatter3D([guide[0]], [guide[1]], [guide[2]], c='b', marker='s')
ax.scatter3D([head_uav0_location[0]], [head_uav0_location[1]], [head_uav0_location[2]], c='b', marker='^')
ax.scatter3D([item[0] for item in uav_list],[item[1] for item in uav_list], [item[2] for item in uav_list], c = "r", marker = "o")

ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')

fig = plt.figure()
plt.show()