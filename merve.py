# At the end of the OSD.py first_line through sixth_line
# change the strings so that you should be able
# to show the status of the crew right of the OSD
# Example my_crew[0].remaining_battery is 750
# In the second_line you can see Battery:IIII
# So you should reformat 750 to 0750 then print
# Similarly, if airspeed is like 5.565486524
# You should reformat it to 05.57 for printing
# in the third_line string values
# Make as many lines as you can by reading like
# my_crew[i].alt for reading the altitude of ith crew
# I means integer, C means character
# III.II means you had to print 3 integers before point
# 2 integer decimals after point
# CCC means you need to put 3 chars to the OSD points
# Search pythons built in function which is format()
# For example: print("{:06.3f}".format(3.1245642)) >>> 03.125
# Means that 3 integer after the point and fill with zeros if
# the restricted number (3.125) has length less than 6
# it has 5 length (count the point . too) so it puts 1 zero
# left side of the printing
# print("{:06.3f}".format(3.1245642)) >>> 03.125
# print("{:07.3f}".format(3.1245642)) >>> 003.125
# print("{:06.4f}".format(3.1245642)) >>> 3.1246
# print("{:010.5f}".format(3.1245642)) >>> 0003.12456

from testucusu.object import UAV

# These are my UAV buddies
my_crew = [UAV() for _ in range(5)]

# Let me place my buddies in map
my_crew[0] = UAV(-5, 10, 1.45456, True, "TKO", "SRC", 0, 5000, 1500, 750, 3.256879, 2.365646, 100, 47.456421, -2.456456,
                 500, 343.564656, 140, 50, 207, 3.4678951)
my_crew[1] = UAV(-5, 11, 11.4546456455, True, "LND", "TRK", 0, 6000, 1600, 850, 4.256879, 3.365646, 200, 57.456421, 3.456456,
                 600, 243.564656, 240, 60, 307, 43.4678951)
my_crew[2] = UAV(-5, 12, 22.12134, True, "RTL", "SRC", 0, 7000, 1700, 950, 5.256879, 4.365646, 300, 67.456421, -4.456456,
                 700, 143.564656, 340, 70, 407, 143.4678951)
my_crew[3] = UAV(-5, 13, 103.45, True, "INT", "TRK", 0, 8000, 1800, 1050, 6.256879, 5.365646, 400, 77.456421, 5.456456,
                 800, 43.564656, 440, 80, 507, 243.4678951)
my_crew[4] = UAV(-5, 14, 104.1, True, "INT", "SRC", 0, 9000, 1900, 1150, 7.256879, 6.365646, 500, 87.456421, -6.456456,
                 900, 3.564656, 540, 90, 607, 343.4678951)

for uav in my_crew:
    # Print all uav telemetry information exactly like you see in the buddy box OSD

    # Print uav remaining of battery in the IIII format, usage of battery in the I.I format, total travelled distance in yhe IIIII format
    print("Battery: " + "{:04.0f}".format(uav.remaining_battery) + "(" + "{:04.0f}".format(uav.total_battery) + ", " + "{:03.1f}".format(uav.battery_usage)+ ") " + "Travel: " + "{:05.0f}".format(uav.travelled))
    # Print uav airspeeds in the II.II format, ground speeds in the II.II format, home distance in the IIIII format
    print("Airspd: " + "{:05.2f}".format(uav.air_speed) + "  GndSpd: " + "{:05.2f}".format(uav.ground_speed) + "  Home: " + "{:05.0f}".format(uav.home))
    #Print uav roll in the -II.I format, pitch in the +II.I format, throttle in the IIII format
    print("Roll: " + "{:05.1f}".format(uav.roll) + "  Pitch: " + "{:05.1f}".format(uav.pitch) + "  Throttle: " + "{:04.0f}".format(uav.throttle))
    #Print uav X in the +IIII format, Y in the -IIII format, altitude in the III.I format, yaw in the III.II format,
    print("X: " + "{:05.0f}".format(uav.x) + "  Y: " + "{:04.0f}".format(uav.y) + "  Alt: " + "{:05.1f}".format(uav.alt) + "  Yaw: " + "{:06.2f}".format(uav.yaw))
    #Print uav next waypoint number in the IIII format, total waypoint number in the IIII format, next waypoint distance in the IIIII format, next waypoint yaw in the III.II format
    print("NWP: " + "{:04.0f}".format(uav.next_waypoint) + "(" + "{:04.0f}".format(uav.total_waypoints) + ") " + "Dist: " + "{:05.0f}".format(uav.next_waypoint_distance)+ "  Yaw: "  + "{:06.2f}".format(uav.next_waypoint_yaw))
    # Do other things, indicate what you print by comment line like above i understand it

    pass
