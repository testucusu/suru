from testucusu.abbreviation import Duty, FormationType, AssetType
import testucusu.configuration as configuration


# two dimensional point in the space
class Point2D:

    def __init__(self, x=0.0, y=0.0):
        """
        2D point class

        :param x: The X coordinate
        :type x: float
        :param y: The Y coordinate
        :type y: float
        """

        self.x = x
        self.y = y


# three dimensional point in the space
class Point3D(Point2D):
    def __init__(self, x=0.0, y=0.0, z=-1.0):
        """
        3D point class

        :param x: The X coordinate
        :type x: float
        :param y: The Y coordinate
        :type y: float
        :param z: The Z coordinate
        :type z: float
        """

        Point2D.__init__(self, x, y)
        self.z = z


# building object class
class Building(Point2D):

    def __init__(self, x=0.0, y=0.0):
        """
        Building class

        :param x: The X coordinate
        :type x: float
        :param y: The Y coordinate
        :type y: float
        """

        Point2D.__init__(self, x, y)


# companion vehicle object class
class Companion(Point3D):

    def __init__(self, number=-1, x=0.0, y=0.0, z=0.0, heading=0.0, x_speed=0.0, y_speed=0.0, duty=Duty.not_defined,
                 simulation_time=0.0):
        """
        Companion class that holds other vehicle data

        :param number: Identification of the companion vehicle
        :type number: int
        :param x: The X coordinate
        :type x: float
        :param y: The Y coordinate
        :type y: float
        :param z: The Z coordinate
        :type z: float
        :param heading: Heading from true north
        :type heading: float
        :param x_speed: Speed on the lateral axis
        :type x_speed: float
        :param y_speed: Speed on the longitudinal axis
        :type y_speed: float
        :param duty: Duty
        :type duty: :class:`testucusu.abbreviation.Duty`
        :param simulation_time: Simulation time when this object was updated
        :type simulation_time: float
        """

        Point3D.__init__(self, x, y, z)
        self.number = number
        self.heading = heading
        self.x_speed = x_speed
        self.y_speed = y_speed
        self.duty = duty
        self.simulation_time = simulation_time


# guide vehicle object class
class Guide(Point3D):

    def __init__(self, x=0.0, y=0.0, z=0.0, heading=0.0, x_speed=0.0, y_speed=0.0, noise=False, dispatch=False,
                 simulation_time=0.0):
        """
        Guide vehicle class

        :param x: The X coordinate
        :type x: float
        :param y: The Y coordinate
        :type y: float
        :param z: The Z coordinate
        :type z: float
        :param heading: Heading
        :type heading: float
        :param x_speed: X Speed
        :type x_speed: float
        :param y_speed: Y Speed
        :type y_speed: float
        :param noise: Noise
        :type noise: bool
        :param dispatch: Dispatch
        :type dispatch: bool
        :param simulation_time: Simulation time
        :type simulation_time: float
        """

        Point3D.__init__(self, x, y, z)
        self.heading = heading
        self.x_speed = x_speed
        self.y_speed = y_speed
        self.noise = noise
        self.dispatch = dispatch
        self.simulation_time = simulation_time


# formation object class
class Formation:

    def __init__(self, distance_guide=0.0, distance_vehicle=0.0, guide_heading=0.0, formation_angle=0.0,
                 formation_type=FormationType.not_defined, simulation_time=0.0):
        """
        Formation class

        :param distance_guide: Guide distance
        :type distance_guide: float
        :param distance_vehicle: Vehicle distance
        :type distance_vehicle: float
        :param guide_heading: Guide heading
        :type guide_heading: float
        :param formation_angle: Formation angle
        :type formation_angle: float
        :param formation_type: Formation type
        :type formation_type: :class:`testucusu.abbreviation.FormationType`
        :param simulation_time: Simulation time
        :type simulation_time: float
        """

        self.distance_guide = distance_guide
        self.distance_vehicle = distance_vehicle
        self.guide_heading = guide_heading
        self.formation_angle = formation_angle
        self.formation_type = formation_type
        self.simulation_time = simulation_time


# person object class
class Casualty(Point2D):

    def __init__(self, x=0.0, y=0.0, casualty_type=AssetType.casualty_unknown, simulation_time=0.0):
        """
        Casualty class

        :param x: The X coordinate
        :type x: float
        :param y: The Y coordinate
        :type y: float
        :param casualty_type: Casualty type
        :type casualty_type: :class:`testucusu.abbreviation.AssetType`
        :param simulation_time: Simulation time
        :type simulation_time: float
        """

        Point2D.__init__(self, x, y)
        self.casualty_type = casualty_type
        self.simulation_time = simulation_time


# hospital object class
class Hospital(Point2D):

    def __init__(self, x=0.0, y=0.0, hospital_quota=-1, simulation_time=0.0):
        """
        Hospital class

        :param x: The X coordinate
        :type x: float
        :param y: The Y coordinate
        :type y: float
        :param hospital_quota: Hospital quota
        :type hospital_quota: int
        :param simulation_time: Simulation time
        :type simulation_time: float
        """

        Point2D.__init__(self, x, y)
        self.hospital_quota = hospital_quota
        self.simulation_time = simulation_time

    # type of the hospital based on it's quota
    @property
    def hospital_type(self):
        """
        Gives the hospital type with :class:`testucusu.abbreviation.AssetType` class
        """

        if self.hospital_quota < 0:
            hospital_type = AssetType.hospital_unknown
        elif self.hospital_quota == 0:
            hospital_type = AssetType.hospital_unavailable
        else:
            hospital_type = AssetType.hospital_available
        return hospital_type


# scenario parameters object class
class Scenario:
    def __init__(self, level_difficulty=0, uav_count=0, uav_communication_range=0.0,
                 telecommunication_altitude_minimum=0.0, telecommunication_altitude_maximum=0.0, telecommunication_range=0.0,
                 extraction_distance_error=0.0, extraction_rescue_time=0.0, extraction_rescue_altitude=0.0,
                 extraction_release_time=0.0, extraction_release_altitude=0.0,
                 sensor_aspect_ratio=0.0, sensor_pitch=0.0, sensor_view_angle=0.0, sensor_altitude_minimum=0.0,
                 sensor_altitude_maximum=0.0, world_length=0.0, world_width=0.0, world_boundaries=None, denied_zones=None,
                 special_assets=None, received=False):
        """
        Scenario parameters class

        :param level_difficulty: Level difficulty
        :type level_difficulty: int
        :param uav_count: Total uav count
        :type uav_count: int
        :param uav_communication_range: Communication range between vehicles
        :type uav_communication_range: float
        :param telecommunication_altitude_minimum: Minimum communication service altitude
        :type telecommunication_altitude_minimum: float
        :param telecommunication_altitude_maximum: Minimum communication service altitude
        :type telecommunication_altitude_maximum: float
        :param telecommunication_range: Communication service range
        :type telecommunication_range: float
        :param extraction_distance_error: Rescue distance error
        :type extraction_distance_error: float
        :param extraction_rescue_time: Rescue getting time
        :type extraction_rescue_time: float
        :param extraction_rescue_altitude: Rescue getting altitude
        :type extraction_rescue_altitude: float
        :param extraction_release_time: Rescue releasing time
        :type extraction_release_time: float
        :param extraction_release_altitude: Rescue releasing altitude
        :type extraction_release_altitude: float
        :param sensor_aspect_ratio: Sensor aspect ratio
        :type sensor_aspect_ratio: float
        :param sensor_pitch: Sensor pitch
        :type sensor_pitch: float
        :param sensor_view_angle: Sensor view angle
        :type sensor_view_angle: float
        :param sensor_altitude_minimum: Minimum sensor altitude
        :type sensor_altitude_minimum: float
        :param sensor_altitude_maximum: Maximum sensor altitude
        :type sensor_altitude_maximum: float
        :param world_length: Simulation environment length
        :type world_length: float
        :param world_width: Simulation environment width
        :type world_width: float
        :param world_boundaries: Simulation environment boundaries
        :type world_boundaries: list
        :param denied_zones: Denied zones
        :type denied_zones: list
        :param special_assets: Special assets
        :type special_assets: list
        :param received: Scenario parameters got flag
        :type received: bool
        """

        self.level_difficulty = level_difficulty
        self.uav_count = uav_count
        self.uav_communication_range = uav_communication_range
        self.telecommunication_altitude_minimum = telecommunication_altitude_minimum
        self.telecommunication_altitude_maximum = telecommunication_altitude_maximum
        self.telecommunication_range = telecommunication_range
        self.extraction_distance_error = extraction_distance_error
        self.extraction_rescue_time = extraction_rescue_time
        self.extraction_rescue_altitude = extraction_rescue_altitude
        self.extraction_release_time = extraction_release_time
        self.extraction_release_altitude = extraction_release_altitude
        self.sensor_aspect_ratio = sensor_aspect_ratio
        self.sensor_pitch = sensor_pitch
        self.sensor_view_angle = sensor_view_angle
        self.sensor_altitude_minimum = sensor_altitude_minimum
        self.sensor_altitude_maximum = sensor_altitude_maximum
        self.world_length = world_length
        self.world_width = world_width
        self.world_boundaries = world_boundaries
        self.denied_zones = denied_zones
        self.special_assets = special_assets
        self.received = received


# companion vehicle object class
class Waypoint(Point3D):

    def __init__(self, x=0.0, y=0.0, z=-1.0, goto_altitude_method=configuration.goto_altitude_method):
        """
        Waypoint class that holds waypoint parameters

        :param x: The X coordinate
        :type x: float
        :param y: The Y coordinate
        :type y: float
        :param z: The Z coordinate
        :type z: float
        :param goto_altitude_method: Altitude method
        :type goto_altitude_method: :class:`testucusu.abbreviation.GotoAltitudeClearingType`
        """

        Point3D.__init__(self, x, y, z)
        self.reached = False
        self.goto_altitude_method = goto_altitude_method
