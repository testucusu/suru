import math
from testucusu.object import Point2D


def clamp(current_value, min_value, max_value):
    """
    Clamps value to desired interval

    :param current_value: Value that wanted to be clamped
    :type current_value: float
    :param min_value: Minimum value that desired as output
    :type min_value: float
    :param max_value: Maximum value that desired as output
    :type max_value: float
    :return: Clamped value
    :rtype: float
    """

    # return to clamped value
    return max(min(current_value, max_value), min_value)


def displacement(x_speed, y_speed, delta_time):
    """
    Calculates estimated displacement based on vector speed and delta time

    :param x_speed: Speed on x axis in m/s
    :type x_speed: float
    :param y_speed: Speed on y axis in m/s
    :type y_speed: float
    :param delta_time: Time spent at current speed in seconds
    :type delta_time: float
    :return: Estimated displacement in meters
    :rtype: float
    """

    # return to calculated displacement
    return (x_speed ** 2 + y_speed ** 2) ** (1 / 2) * delta_time


def battery_usage(x_speed, y_speed):
    """
    Calculates instant battery usage

    :param x_speed: Speed on x axis in knots
    :type x_speed: float
    :param y_speed: Speed on y axis in knots
    :type y_speed: float
    :return: Battery usage in liters/second
    :rtype: float
    """

    # calculate vector sum of speeds
    vector = (x_speed ** 2 + y_speed ** 2) ** (1 / 2)
    vector *= 1.94384449

    # calculate usage
    usage = ((vector - 60.0) ** 2) / 9.0 + 500
    usage *= 0.000125997881

    # return to usage
    return usage


def distance_2d(first_point, second_point):
    """
    Calculates distance between two waypoints

    :param first_point: First waypoint
    :type first_point: :class:`testucusu.object.Point2D`
    :param second_point: Second waypoint
    :type second_point: :class:`testucusu.object.Point2D`
    :return: Distance between waypoints in meters
    :rtype: float
    """

    # get points
    (x1, y1) = (first_point.x, first_point.y)
    (x2, y2) = (second_point.x, second_point.y)

    # return to calculated distance
    return (((x2 - x1) ** 2) + ((y2 - y1) ** 2)) ** (1 / 2)


def distance_3d(first_point, second_point):
    """
    Calculates distance between two waypoints

    :param first_point: First waypoint
    :type first_point: :class:`testucusu.object.Point3D`
    :param second_point: Second waypoint
    :type second_point: :class:`testucusu.object.Point3D`
    :return: Distance between waypoints in meters
    :rtype: float
    """

    # get points
    (x1, y1, z1) = (first_point.x, first_point.y, first_point.z)
    (x2, y2, z2) = (second_point.x, second_point.y, second_point.z)

    # return to calculated distance
    return (((x2 - x1) ** 2) + ((y2 - y1) ** 2) + ((z2 - z1) ** 2)) ** (1 / 2)


def bearing(first_point, second_point):
    """
    Calculates bearing from first point to second point by true north

    :param first_point: First waypoint
    :type first_point: tuple[float, float]
    :param second_point: Second waypoint
    :type second_point: tuple[float, float]
    :return: Bearing from first point to second point by true north in degrees
    :rtype: float
    """

    # calculate and return bearing from first point to second point by true north
    angle = math.degrees(
        math.atan2(1.0, 0.0) - math.atan2(second_point[1] - first_point[1], second_point[0] - first_point[0]))

    # return to bearing angle
    return angle + 360.0 if angle < 0.0 else angle


def destination(start_point, heading, distance_to_go):
    """
    Calculates destination point given that starting point, heading by true north and distance to go

    :param start_point: First waypoint
    :type start_point: tuple[float, float]
    :param heading: Heading by true north
    :type heading: float
    :param distance_to_go: Distance to go
    :type distance_to_go: float
    :return: Destination point
    :rtype: tuple[float, float]
    """

    # calculate and return destination point
    x = start_point[0] + distance_to_go * math.cos(math.radians(90 - heading))
    y = start_point[1] + distance_to_go * math.sin(math.radians(90 - heading))

    # return to destination point
    return x, y


def rotate(rotating_point, center_point, angle=0.0):
    """
    Rotates given point around center point by given angle

    :param rotating_point: Rotating point
    :type rotating_point: :class:`testucusu.object.Point2D`
    :param center_point: Center point
    :type center_point: :class:`testucusu.object.Point2D`
    :param angle: Rotation angle
    :type angle: float
    :return: Destination point
    :rtype: :class:`testucusu.object.Point2D`
    """

    # convert angle to radians
    radians = math.radians(angle)

    # calculate final x
    x = center_point.x + math.cos(radians) * (rotating_point.x - center_point.x) + math.sin(radians) * (
                rotating_point.y - center_point.y)

    # calculate final y
    y = center_point.y - math.sin(radians) * (rotating_point.x - center_point.x) + math.cos(radians) * (
                rotating_point.y - center_point.y)

    # return final point as object
    return Point2D(x, y)
