import time
import uuid
import pickle
import pika


# communication class
class Communicator:
    def __init__(self, host="127.0.0.1", port=5672):
        self.host = host
        self.port = port
        self.queue_callbacks = {}
        self.request_callbacks = {}
        self.response = None
        self.corr_id = None
        self.request_queues_initialized = False
        self.connection = None
        self.channel = None
        self.response_queue = None

    def connect(self):
        credentials = pika.PlainCredentials('teknofest', 'teknofest')
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=self.host, port=self.port, credentials=credentials, heartbeat=0))
        self.channel = self.connection.channel()
        self.channel.basic_qos(prefetch_count=1)

    def init_request_queues(self):
        if not self.request_queues_initialized:
            queue_declaration = self.channel.queue_declare(queue='', auto_delete=True)
            self.response_queue = queue_declaration.method.queue
            self.channel.basic_consume(queue=self.response_queue,
                                       on_message_callback=self.on_response,
                                       auto_ack=True)
            self.request_queues_initialized = True

    def declare_queue(self, queue_name):
        try:
            self.channel.queue_declare(queue=queue_name,
                                       auto_delete=True,
                                       arguments={"x-max-length": 1,
                                                  "x-overflow": "drop-head"})
        except pika.exceptions.ChannelClosedByBroker as ex:
            # make sure that if we are redefining one of the queues,
            # we reconnect to the channel and delete the previous queue definition
            # also redefine the queue
            print('ERROR:queue declaration error!')
            print('Reconnecting the channel')
            self.connect()
            # delete the queue for definition
            self.channel.queue_delete(queue_name)
            self.declare_queue(queue_name)
        return True

    def register_to_queue(self, queue_name, user_callback):
        if self.declare_queue(queue_name):
            self.channel.basic_consume(queue=queue_name,
                                       on_message_callback=self.queue_callback,
                                       auto_ack=True)
            self.queue_callbacks[queue_name] = user_callback

    def queue_callback(self, ch, method, props, body):
        if type(body) is str:
            try:
                body = pickle.loads(body)
            except:
                pass
        self.queue_callbacks[method.routing_key](body)

    def register_queue(self, queue_name):
        self.declare_queue(queue_name)

    def send(self, queue_name, data):
        if type(data) is not str:
            data = pickle.dumps(data, protocol=2)

        self.channel.basic_publish(exchange='',
                                   routing_key=queue_name,
                                   body=data)

    def register_request(self, request_name, request_callback):
        self.init_request_queues()
        self.channel.queue_declare(queue=request_name)
        self.channel.basic_consume(queue=request_name, on_message_callback=self.on_request)
        self.request_callbacks[request_name] = request_callback

    def on_request(self, ch, method, props, body):
        response = self.request_callbacks[method.routing_key](body)
        if type(response) is not str:
            response = pickle.dumps(response, protocol=2)

        new_props = pika.BasicProperties(correlation_id=props.correlation_id)
        ch.basic_publish(exchange='',
                         routing_key=props.reply_to,
                         properties=new_props,
                         body=response)
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def is_request_registered(self, request_name):
        try:
            queue_declaration = self.channel.queue_declare(queue=request_name)
        except ValueError as ex:
            return False
        if queue_declaration.method.consumer_count > 0:
            return True
        else:
            self.channel.queue_delete(request_name)
            return False

    def send_request(self, request_name, params=''):
        self.init_request_queues()
        if not self.is_request_registered(request_name):
            print("ERROR: Not registered request:" + request_name)
            return None
        if type(params) is not str:
            params = pickle.dumps(params, protocol=2)
        self.response = None
        self.corr_id = str(uuid.uuid4())
        props = pika.BasicProperties(reply_to=self.response_queue, correlation_id=self.corr_id)
        self.channel.basic_publish(exchange='',
                                   routing_key=request_name,
                                   properties=props,
                                   body=params)
        while self.response is None:
            self.connection.process_data_events()
            time.sleep(0.0001)
        return self.response

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            if type(body) is str:
                try:
                    body = pickle.loads(body)
                except:
                    pass
            self.response = body

    def start_listening(self):
        self.channel.start_consuming()

