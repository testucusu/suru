import time
import threading
import pickle
import shapely.geometry as shp
from testucusu.object import Point2D, Point3D, Companion, Guide, Formation, Casualty, Hospital, Scenario, Waypoint
from testucusu.communication import Communicator
from testucusu.abbreviation import FlightMode, Duty, FormationType, AssetType, AltitudeClearingType, \
    GotoAltitudeClearingType, SearchAndRescueEndingType
import testucusu.configuration as configuration
from testucusu.util import clamp, displacement, battery_usage, bearing, distance_2d, distance_3d
from testucusu.navigation import path_creator, avoidance, no_fly_zone_creator


# UAV object class
class UAV(Point3D):

    def __init__(self, number=-1, name="Noname", server_address="127.0.0.1", server_port=5672):
        """
        UAV vehicle class

        :param number: Number of the vehicle
        :type number: int
        :param name: User friendly vehicle name
        :type name: str
        :param server_address: Server address
        :type server_address: str
        :param server_port: Server port
        :type server_port: int
        """

        # 3D location of the UAV
        Point3D.__init__(self, x=0.0, y=0.0, z=-1.0)

        # identity
        self.number = number
        self.name = name

        # main status
        self.armed = False
        """
        Enables user to send commands to vehicle
        """
        self.mode = FlightMode.not_defined
        """
        Flight mode of the vehicle using :class:`testucusu.abbreviation.FlightMode`
        """
        self.duty = Duty.not_defined
        """
        Flight duty of the vehicle using :class:`testucusu.abbreviation.Duty`
        """
        self.support = -1
        """
        Support id of the other vehicle that will be helped
        """
        self.safety_switch = False
        """
        Safety switch to check got telemetry from vehicle
        """

        # battery related attributes
        self.battery_capacity = 0.0
        """
        Initial battery capacity of the vehicle
        """
        self.battery_remaining = 0.0
        """
        Remaining battery capacity of the vehicle
        """

        # casualty related attributes
        self.casualty_carrying = False
        """
        Casualty carrying flag, True if vehicle carrying a casualty
        """
        self.casualty_extracted_count = 0
        """
        Total number of casualties extracting since startup
        """
        self.casualties = []
        """
        Casualty list both healthy and injured
        """
        self.enable_rescue = False
        """
        Enable rescue if injured casualty was found during search mission
        """

        # hospital related attributes
        self.hospitals = []
        """
        Hospital list in the simulation environment
        """

        # telecommunication related attributes
        self.telecommunication_service_count = 0
        """
        Live number of casualties those will be served with telecommunication services
        """

        # companion vehicle related attributes
        self.companion_vehicles = []
        """
        Companion vehicle list
        """

        # formation flight related attributes
        self.guide = Guide()
        """
        Guide vehicle
        """
        self.formation = Formation()
        """
        Formation related parameters
        """

        # scenario related attributes
        self.scenario = Scenario()
        """
        Scenario parameters of the simulation environment
        """

        # navigation related attributes
        self.next_waypoint = Waypoint()
        """
        Next waypoint in :class:`testucusu.object.Waypoint` class
        """
        self.waypoints = None
        """
        Calculated waypoint list
        """
        self.waypoint_next_count = -1
        """
        Next target waypoint number
        """
        self.search_cycle_count = 0
        """
        Number of cycles that current waypoint list followed
        """

        # travelled distance
        self.travelled_distance_2d = 0.0
        """
        Travelled distance based on 2d movement
        """
        self.travelled_distance_3d = 0.0
        """
        Travelled distance based on 3d movement
        """

        # exposed miscellaneous
        self.simulation_time_current = 0.0
        """
        Current simulation time
        """
        self.simulation_time_delta = 0.0
        """
        Live telemetry delay
        """

        # unexposed commands and readings
        self.__x_speed = 0.0
        self.__y_speed = 0.0
        self.__altitude = 0.0
        self.__heading = 0.0
        self.__command_x_speed = 0.0
        self.__command_y_speed = 0.0
        self.__command_altitude = 0.0
        self.__command_heading = 0.0

        # unexposed miscellaneous
        self.__simulation_starting_time = 0.0
        self.__mode_previous = FlightMode.not_defined
        self.__goto_altitude_method = configuration.goto_altitude_method
        self.__search_and_rescue_ending_method = configuration.search_and_rescue_ending_method
        self.__goto_maintaining_altitude = 0.0
        self.__fallback_waypoint_count = -1
        self.__collision_avoidance_altitude_offset = 0
        self.__collision_companions = None
        self.__last_casualty_extracted_count = 0
        self.__casualty_rescue = None
        self.__hospital_closest = None

        # communication
        self.__server_address = server_address
        self.__server_port = server_port
        self.__communication = None
        self.__telemetry_message_raw = None
        self.__telemetry_message = None
        self.__scenario_parameters_raw = None
        self.__scenario_parameters = None

        # start connect thread
        connect_thread = threading.Thread(target=self.__connect)
        connect_thread.start()

        # start telemetry parser thread
        parser_thread = threading.Thread(target=self.__parser)
        parser_thread.start()

        # mode handler thread
        mode_watchdog_thread = threading.Thread(target=self.__mode_watchdog)
        mode_watchdog_thread.start()

    # x speed getter
    @property
    def x_speed(self):
        """
        Get vehicle x speed

        :return: Lateral speed of the vehicle
        :rtype: float
        """
        return self.__x_speed

    # x speed setter
    @x_speed.setter
    def x_speed(self, val):
        """
        Set vehicle x speed
        """
        self.__command_x_speed = val

    # y speed getter
    @property
    def y_speed(self):
        """
        Get vehicle y speed

        :return: Longitudinal speed of the vehicle
        :rtype: float
        """
        return self.__y_speed

    # y speed setter
    @y_speed.setter
    def y_speed(self, val):
        """
        Set vehicle y speed
        """
        self.__command_y_speed = val

    # altitude getter
    @property
    def altitude(self):
        """
        Get vehicle altitude

        :return: Altitude of the vehicle
        :rtype: float
        """
        return self.__altitude

    # altitude setter
    @altitude.setter
    def altitude(self, val):
        """
        Set vehicle altitude
        """
        self.__command_altitude = val + self.__collision_avoidance_altitude_offset

    # heading getter
    @property
    def heading(self):
        """
        Get vehicle heading

        :return: Heading of the vehicle
        :rtype: float
        """
        return self.__heading

    # heading setter
    @heading.setter
    def heading(self, val):
        """
        Set vehicle heading
        """
        self.__command_heading = val

    # battery level by percentage property
    @property
    def battery_remaining_percentage(self):
        """
        Get remaining battery as percentage

        :return: Remaining battery as percentage
        :rtype: float
        """
        return self.battery_remaining / self.battery_capacity * 100.0

    # used battery level
    @property
    def battery_used(self):
        """
        Get used battery

        :return: Used battery
        :rtype: float
        """
        return self.battery_capacity - self.battery_remaining

    # used battery level percentage
    @property
    def battery_used_percentage(self):
        """
        Get used battery as percentage

        :return: Used battery as percentage
        :rtype: float
        """
        return self.battery_used / self.battery_capacity * 100.0

    # battery usage per unit movement in 2D
    @property
    def battery_usage_movement_2d(self):
        """
        Get battery usage per unit 2d movement

        :return: Battery usage per unit 2d movement
        :rtype: float
        """
        return self.battery_used / self.travelled_distance_2d if self.travelled_distance_2d > 0.0 else 0.0

    # battery usage per unit movement in 3D
    @property
    def battery_usage_movement_3d(self):
        """
        Get battery usage per unit 3d movement

        :return: Battery usage per unit 3d movement
        :rtype: float
        """
        return self.battery_used / self.travelled_distance_3d if self.travelled_distance_3d > 0.0 else 0.0

    # battery usage per second based on measurements
    @property
    def battery_usage_time_measured(self):
        """
        Get measured battery usage per time

        :return: Measured battery usage per time
        :rtype: float
        """
        return self.battery_used / self.simulation_time_passed if self.simulation_time_passed > 0.0 else 0.0

    # battery usage per second based on technical document
    @property
    def battery_usage_time_calculated(self):
        """
        Get calculated battery usage per time

        :return: Calculated battery usage per time
        :rtype: float
        """

        # there is no noise inside environment
        if not self.guide.noise:

            # calculate battery usage using x and y speed
            battery_usage_current = battery_usage(self.x_speed, self.y_speed)

        # there is noise inside environment so estimate current battery usage
        else:

            # estimate battery usage current based on commands
            battery_usage_current = battery_usage(self.__command_x_speed, self.__command_y_speed)

        return battery_usage_current

    # estimated remaining flight time reserved based on measured battery usage
    @property
    def remaining_time_measured(self):
        """
        Get remaining time based on measured battery usage

        :return: Remaining time based on measured battery usage
        :rtype: float
        """

        # return to calculated value
        return self.battery_remaining / self.battery_usage_time_measured if self.battery_usage_time_measured > 0.0 else 0.0

    # estimated remaining flight time reserved based on calculated battery usage
    @property
    def remaining_time_calculated(self):
        """
        Get remaining time based on calculated battery usage

        :return: Remaining time based on measured battery usage
        :rtype: float
        """

        # return to calculated value
        return self.battery_remaining / self.battery_usage_time_calculated if self.battery_usage_time_calculated > 0.0 else 0.0

    # estimated remaining flight distance reserved based on 2D movement battery usage
    @property
    def remaining_distance_2d(self):
        """
        Get estimated 2d distance left based on battery usage in 2d movement

        :return: Estimated 2d distance left based on battery usage in 2d movement
        :rtype: float
        """

        # return to calculated value
        return self.battery_remaining / self.battery_usage_movement_2d if self.battery_usage_movement_2d > 0.0 else 0.0

    # estimated remaining flight distance reserved based on calculated battery usage
    @property
    def remaining_distance_3d(self):
        """
        Get estimated 3d distance left based on battery usage in 3d movement

        :return: Estimated 3d distance left based on battery usage in 3d movement
        :rtype: float
        """

        # return to calculated value
        return self.battery_remaining / self.battery_usage_movement_3d if self.battery_usage_movement_3d > 0.0 else 0.0

    # passed simulation time property
    @property
    def simulation_time_passed(self):
        """
        Get simulation time passed

        :return: Simulation time passed
        :rtype: float
        """

        # return to passed simulation time
        return self.simulation_time_current - self.__simulation_starting_time

    # takeoff cleared flag property for convenience
    @property
    def takeoff_cleared(self):
        """
        Get takeoff cleared flag

        :return: Takeoff cleared flag
        :rtype: bool
        """

        # return to flag
        return self.target_altitude_reached

    # target altitude reached flag
    @property
    def target_altitude_reached(self):
        """
        Get target altitude reached flag

        :return: Target altitude reached flag
        :rtype: bool
        """

        # default target altitude reached flag is false
        target_altitude_reached = False

        # clearing method is percentage
        if configuration.altitude_clearing_method == AltitudeClearingType.percentage:
            if abs(self.__command_altitude - self.z) < 0.01 * self.altitude * \
                    configuration.altitude_clearing_error_percentage:
                target_altitude_reached = True

        # clearing method is offset
        elif configuration.altitude_clearing_method == AltitudeClearingType.offset:
            if abs(self.__command_altitude - self.z) < configuration.altitude_clearing_error_offset:
                target_altitude_reached = True

        # return to calculated flag
        return target_altitude_reached

    # target reached 2D flag
    @property
    def waypoint_reached_2d(self):
        """
        Get waypoint reached 2d flag

        :return: Waypoint reached 2d flag
        :rtype: bool
        """

        # default target reached flag
        target_reached_2d = configuration.target_reached_distance_default > self.waypoint_distance_2d

        # target reached flag in search mode
        if self.mode == FlightMode.search_and_rescue:
            target_reached_2d = configuration.target_reached_distance_search > self.waypoint_distance_2d

            # vehicle going towards casualty or hospital
            if self.__casualty_rescue is not None or self.__hospital_closest is not None:

                # vehicle is above casualty or hospital
                if self.waypoint_total_count - self.waypoint_next_count < 3:
                    # approach flag will be set carefully
                    target_reached_2d = configuration.target_reached_distance_default > self.waypoint_distance_2d

        # return to flag
        return target_reached_2d

    # target reached 3D flag
    @property
    def waypoint_reached_3d(self):
        """
        Get waypoint reached 3d flag

        :return: Waypoint reached 3d flag
        :rtype: bool
        """

        # return to flag
        return self.waypoint_reached_2d and self.target_altitude_reached

    # calculated target heading property
    @property
    def waypoint_heading(self):
        """
        Get waypoint heading

        :return: Waypoint heading
        :rtype: float
        """

        # return to heading
        return bearing((self.x, self.y), (self.next_waypoint.x, self.next_waypoint.y))

    # calculated target heading error property
    @property
    def waypoint_heading_error(self):
        """
        Get waypoint heading error

        :return: Waypoint heading error
        :rtype: float
        """

        # return to heading error
        return self.waypoint_heading - self.heading

    # calculated 2D target distance
    @property
    def waypoint_distance_2d(self):
        """
        Get waypoint 2d distance

        :return: Waypoint 2d distance
        :rtype: float
        """

        # return to distance
        return distance_2d(Point2D(self.x, self.y), Point2D(self.next_waypoint.x, self.next_waypoint.y))

    # calculated 3D target distance
    @property
    def waypoint_distance_3d(self):
        """
        Get waypoint 3d distance

        :return: Waypoint 3d distance
        :rtype: float
        """

        # return to distance
        return distance_3d(Point3D(self.x, self.y, self.z),
                           Point3D(self.next_waypoint.x, self.next_waypoint.y, self.next_waypoint.z))

    # target is in the approaching range
    @property
    def waypoint_approached(self):
        """
        Get waypoint approached flag

        :return: Waypoint approached flag
        :rtype: bool
        """

        # return to flag
        return configuration.target_approaching_distance > self.waypoint_distance_2d

    # target is in the precise approaching range
    @property
    def waypoint_approached_precise(self):
        """
        Get waypoint approached precise flag

        :return: Waypoint approached precise flag
        :rtype: bool
        """

        # return to flag
        return configuration.target_precise_distance > self.waypoint_distance_2d

    # total waypoint count
    @property
    def waypoint_total_count(self):
        """
        Get total waypoint number

        :return: Total waypoint number
        :rtype: int
        """

        # return to length of waypoint list
        return len(self.waypoints)

    # collision companions
    @property
    def collision_companions(self):
        """
        Get collision companion vehicle number list

        :return: Collision companion vehicle number list
        :rtype: list
        """

        # return to list
        return self.__collision_companions

    # possible collision
    @property
    def possible_collision(self):
        """
        Get possible collision flag

        :return: Possible collision flag
        :rtype: bool
        """

        # calculate flag
        possible_collision = False
        if self.__collision_companions is not None:
            if self.__collision_companions > []:
                possible_collision = True

        # return to flag
        return possible_collision

    # healthy casualties
    @property
    def casualties_healthy(self):
        """
        Get healthy casualty list

        :return: Healthy casualty list
        :rtype: list of :class:`testucusu.object.Casualty`
        """

        # return to list
        return [casualty for casualty in self.casualties if casualty.casualty_type == AssetType.casualty_healthy]

    # injured casualties
    @property
    def casualties_injured(self):
        """
        Get injured casualty list

        :return: Injured casualty list
        :rtype: list of :class:`testucusu.object.Casualty`
        """

        return [casualty for casualty in self.casualties if casualty.casualty_type == AssetType.casualty_injured]

    # goto hospital flag
    @property
    def __goto_hospital(self):
        return self.casualty_carrying and self.__hospital_closest is None

    # goto casualty flag
    @property
    def __goto_casualty(self):
        return self.casualties_injured > [] and self.guide.dispatch and not self.casualty_carrying and self.__casualty_rescue is None

    # summed up command to be sent to server
    @property
    def __command(self):
        return {"x_speed": self.__command_x_speed * 1.94384449, "y_speed": self.__command_y_speed * 1.94384449,
                "altitude": self.__command_altitude, "heading": 180.0 - self.__command_heading,
                "task": "D"}

    # will be used to update the telemetry of the UAV
    def __update(self, uav_msg):
        self.__telemetry_message_raw = uav_msg

        # only send a command if vehicle is armed
        if self.armed:
            self.__communication.send("uav_command_queue_" + str(self.number), self.__command)

    # get scenario parameters from server
    def __get_scenario_parameters(self):
        self.__scenario_parameters_raw = self.__communication.send_request("scenario_parameters")

        try:
            # try to load raw scenario parameters message
            self.__scenario_parameters = pickle.loads(self.__scenario_parameters_raw)

            # get level difficulty
            self.scenario.level_difficulty = int(self.__scenario_parameters["level_difficulty"])

            # get navigation related parameters
            self.scenario.world_length = float(self.__scenario_parameters["world_length"])
            self.scenario.world_width = float(self.__scenario_parameters["world_width"])
            self.scenario.world_boundaries = list(self.__scenario_parameters["world_boundaries"])

            # get denied zones
            self.scenario.special_assets = list(self.__scenario_parameters["special_assets"])
            self.scenario.denied_zones = list(self.__scenario_parameters["denied_zones"])

            # get uav related parameters
            self.scenario.uav_count = int(self.__scenario_parameters["uav_count"])
            self.scenario.uav_communication_range = float(self.__scenario_parameters["uav_communication_range"])

            # get telecommunication related parameters
            self.scenario.telecommunication_range = float(self.__scenario_parameters["telecom_radius"])
            self.scenario.telecommunication_altitude_minimum = float(self.__scenario_parameters["telecom_height_min"])
            self.scenario.telecommunication_altitude_maximum = float(self.__scenario_parameters["telecom_height_max"])

            # get extraction related parameters
            self.scenario.extraction_distance_error = float(self.__scenario_parameters["injured_error_range"])
            self.scenario.extraction_rescue_altitude = float(self.__scenario_parameters["injured_pick_up_height"])
            self.scenario.extraction_rescue_time = float(self.__scenario_parameters["injured_pick_up_duration"])
            self.scenario.extraction_release_altitude = float(self.__scenario_parameters["injured_release_height"])
            self.scenario.extraction_release_time = float(self.__scenario_parameters["injured_release_duration"])

            # get sensor related parameters
            self.scenario.sensor_altitude_minimum = float(self.__scenario_parameters["logical_camera_height_min"])
            self.scenario.sensor_altitude_maximum = float(self.__scenario_parameters["logical_camera_height_max"])
            self.scenario.sensor_pitch = float(self.__scenario_parameters["logical_camera_pitch"])
            self.scenario.sensor_view_angle = float(self.__scenario_parameters["logical_camera_horizontal_fov"])
            self.scenario.sensor_aspect_ratio = float(self.__scenario_parameters["logical_camera_aspect_ratio"])

            # set got scenario parameters flag
            self.scenario.received = True

        except Exception as exp:
            print("Error occurred during scenario parameters message parsing")

    # connect to the server for sending and receiving data
    def __connect(self):
        self.__communication = Communicator(self.__server_address, self.__server_port)
        self.__communication.connect()
        self.__communication.register_to_queue("uav_imu_queue_" + str(self.number), self.__update)
        self.__get_scenario_parameters()
        self.__communication.start_listening()

    # parse incoming telemetry data and update class attributes
    def __parser(self):

        # run over and over
        while True:

            # protect link
            try:

                # try to load raw telemetry message
                self.__telemetry_message = pickle.loads(self.__telemetry_message_raw)

                # get active uav primary data
                self.__x_speed = float(self.__telemetry_message["active_uav"]["x_speed"]) * 0.514444444
                self.__y_speed = float(self.__telemetry_message["active_uav"]["y_speed"]) * 0.514444444
                self.__altitude = float(self.__telemetry_message["active_uav"]["altitude"])
                self.__heading = 180.0 - float(self.__telemetry_message["active_uav"]["heading"])

                # get location of the uav
                x = float(self.__telemetry_message["active_uav"]["location"][0])
                y = float(self.__telemetry_message["active_uav"]["location"][1])
                z = float(self.__telemetry_message["active_uav"]["altitude"])

                # get battery
                remaining_battery = float(self.__telemetry_message["active_uav"]["fuel_reserve"]) * 0.45359237
                if remaining_battery > self.battery_capacity:
                    self.battery_capacity = remaining_battery
                self.battery_remaining = remaining_battery

                # get time
                simulation_time = float(self.__telemetry_message["sim_time"]) / 1000
                if self.__simulation_starting_time == 0.0 or self.simulation_time_passed < 0.0:
                    self.__simulation_starting_time = simulation_time
                if self.simulation_time_current != 0.0:
                    self.simulation_time_delta = simulation_time - self.simulation_time_current
                self.simulation_time_current = simulation_time

                # get duty
                self.duty = Duty(self.__telemetry_message["active_uav"]["task"])

                # get casualty
                self.casualty_carrying = bool(
                    self.__telemetry_message["active_uav"]["injured_rescue_status"]["isCarrying"])
                self.casualty_extracted_count = int(
                    self.__telemetry_message["active_uav"]["injured_rescue_status"]["successfulDrop"])

                # get telecommunication
                self.telecommunication_service_count = int(
                    self.__telemetry_message["active_uav"]["equipments"]["telecom_beacon"][
                        "telecom_served_people_count"])

                # get companion vehicles
                companion_vehicles = self.__telemetry_message["uav_link"]
                for vehicle in companion_vehicles:

                    # parse individual companion vehicle data
                    [[vehicle_name, vehicle_data]] = vehicle.items()
                    vehicle_number = int(vehicle_name.split("_")[1])

                    # rest of the parsing and registering should be only done for companion vehicles
                    if vehicle_number != self.number:
                        vehicle_location_x = float(vehicle_data["location"][0])
                        vehicle_location_y = float(vehicle_data["location"][1])
                        vehicle_location_z = float(vehicle_data["altitude"])
                        vehicle_heading = 180.0 - float(vehicle_data["heading"])
                        vehicle_speed_x = float(vehicle_data["speed"]["x"]) * 0.514444444
                        vehicle_speed_y = float(vehicle_data["speed"]["y"]) * 0.514444444
                        vehicle_duty = Duty(vehicle_data["task"])

                        # create new companion vehicle
                        new_companion_vehicle = Companion(number=vehicle_number, x=vehicle_location_x,
                                                          y=vehicle_location_y, z=vehicle_location_z,
                                                          heading=vehicle_heading, x_speed=vehicle_speed_x,
                                                          y_speed=vehicle_speed_y, duty=vehicle_duty,
                                                          simulation_time=simulation_time)
                        # update companion vehicle list
                        try:

                            # check if this companion vehicle registered before
                            index = [x.number for x in self.companion_vehicles].index(vehicle_number)

                            # if exists, then update the object
                            self.companion_vehicles[index] = new_companion_vehicle

                        # companion vehicle is not registered before
                        except ValueError:

                            # append new companion vehicle to the list
                            self.companion_vehicles.append(new_companion_vehicle)

                # get guide vehicle data
                guide_x = float(self.__telemetry_message["uav_guide"]["location"][0])
                guide_y = float(self.__telemetry_message["uav_guide"]["location"][1])
                guide_z = float(self.__telemetry_message["uav_guide"]["altitude"])
                guide_x_speed = float(self.__telemetry_message["uav_guide"]["speed"]["x"]) * 0.514444444
                guide_y_speed = float(self.__telemetry_message["uav_guide"]["speed"]["y"]) * 0.514444444
                guide_heading = 180.0 - float(self.__telemetry_message["uav_guide"]["heading"])
                guide_noise = bool(self.__telemetry_message["uav_guide"]["gps_noise_flag"])
                guide_dispatch = bool(self.__telemetry_message["uav_guide"]["dispatch"])
                self.guide = Guide(x=guide_x, y=guide_y, z=guide_z, heading=guide_heading, x_speed=guide_x_speed,
                                   y_speed=guide_y_speed, noise=guide_noise, dispatch=guide_dispatch,
                                   simulation_time=simulation_time)

                # get formation parameters
                formation_distance_guide = float(self.__telemetry_message["uav_formation"]["u_k"])
                formation_distance_vehicle = float(self.__telemetry_message["uav_formation"]["u_b"])
                formation_guide_heading = 180.0 - float(self.__telemetry_message["uav_formation"]["a_k"])
                formation_type = FormationType(self.__telemetry_message["uav_formation"]["type"])
                if formation_type == FormationType.arrow:
                    formation_angle = float(self.__telemetry_message["uav_formation"]["a_b"]) * 2
                else:
                    formation_angle = 0.0
                self.formation = Formation(distance_guide=formation_distance_guide,
                                           distance_vehicle=formation_distance_vehicle,
                                           guide_heading=formation_guide_heading, formation_angle=formation_angle,
                                           formation_type=formation_type, simulation_time=simulation_time)

                # get casualties in the world data
                casualties = self.__telemetry_message["casualties_in_world"]

                # for each casualty detected by the vehicle
                for casualty in casualties:

                    # get fields
                    casualty_x = float(casualty["pose"][0])
                    casualty_y = float(casualty["pose"][1])
                    casualty_type = casualty["type"]
                    casualty_status = casualty["status"]
                    casualty_present = bool(casualty["in_world"])

                    # check is casualty is really casualty
                    if casualty_type == "casualty":

                        # casualty is healthy
                        if casualty_status == "healthy":
                            asset_type = AssetType.casualty_healthy

                        # casualty is injured
                        else:
                            asset_type = AssetType.casualty_injured

                        # create person object
                        new_casualty = Casualty(x=casualty_x, y=casualty_y, casualty_type=asset_type,
                                                simulation_time=simulation_time)

                        # update casualty list
                        try:

                            # check if this casualty registered before
                            index = [(x.x, x.y) for x in self.casualties].index((new_casualty.x, new_casualty.y))

                            # if it exists in the list and in the world update it
                            if casualty_present:
                                self.casualties[index] = new_casualty

                            # if it exists in the list but not in the world anymore, delete it from the list
                            else:
                                del self.casualties[index]

                        # casualty is not registered before
                        except ValueError:

                            # append new casualty to the list if it is in the world
                            if casualty_present:
                                self.casualties.append(new_casualty)

                    # bad field or data in casualties
                    else:
                        print("There is an unknown type in casualties in world but skipping it")

                # get hospitals in range data
                hospitals = self.__telemetry_message["hospitals_in_range"]
                for hospital in hospitals:
                    hospital_x = float(hospital["location"][0])
                    hospital_y = float(hospital["location"][1])
                    hospital_quota = int(hospital["quota"])

                    new_hospital = Hospital(x=hospital_x, y=hospital_y, hospital_quota=hospital_quota,
                                            simulation_time=simulation_time)

                    # update hospital list
                    try:

                        # check if this hospital registered before
                        index = [(x.x, x.y) for x in self.hospitals].index((new_hospital.x, new_hospital.y))

                        # if exists, then update the hospital if it is not unavailable (available or unknown)
                        if new_hospital.hospital_type != AssetType.hospital_unavailable:
                            self.hospitals[index] = new_hospital

                        # this hospital can not help anymore, delete it
                        else:
                            del self.hospitals[index]

                    # companion vehicle is not registered before
                    except ValueError as val:

                        # add the hospital to the list if it is not unavailable (available or unknown)
                        if new_hospital.hospital_type != AssetType.hospital_unavailable:
                            self.hospitals.append(new_hospital)

                # get logical camera sensor detected data
                # pprint.pprint(self.__telemetry_message["active_uav"]["sensors"]["logical_camera"]["detected"])

                # location is valid
                if self.z != -1.0:

                    # there is no noise inside environment so it is safe to calculate travelled distances
                    if not self.guide.noise:
                        self.travelled_distance_2d += distance_2d(Point2D(x, y), Point2D(self.x, self.y))
                        self.travelled_distance_3d += distance_3d(Point3D(x, y, z), Point3D(self.x, self.y, self.z))

                    # there is noise inside environment so estimate travelled distance
                    else:

                        # estimate displacement based on speed commands and delta time
                        travelled_distance = displacement(self.__command_x_speed, self.__command_y_speed,
                                                          self.simulation_time_delta)
                        self.travelled_distance_2d += travelled_distance
                        self.travelled_distance_3d += travelled_distance

                # update current position of vehicle
                self.x = x
                self.y = y
                self.z = z

                # got telemetry from vehicle so enable safety switch
                self.safety_switch = True

            except Exception as exp:
                print("Error occurred during telemetry message parsing", exp)

            # cooling down
            time.sleep(0.0001)

    # mode and safety watchdog
    def __mode_watchdog(self):

        # keep watchdog alive
        while True:

            # takeoff watchdog procedures for takeoff mode
            if self.mode == FlightMode.takeoff:
                # set commands using exposed properties
                self.x_speed = 0.0
                self.y_speed = 0.0
                self.altitude = self.next_waypoint.z

            # goto watchdog procedures for goto mode
            if self.mode == FlightMode.goto or self.mode == FlightMode.search_and_rescue:

                # check if reached to waypoint or not
                if self.waypoint_reached_3d:
                    self.next_waypoint.reached = True

                    # if flight mode is search
                    if self.mode == FlightMode.search_and_rescue:

                        # set reached flag of the target location in the target location list
                        self.waypoints[self.waypoint_next_count].reached = True

                        # if this is not the end of the list
                        if self.waypoint_next_count < len(self.waypoints) - 2:

                            # set next target location
                            self.waypoint_next_count += 1

                        # we are at the end of the list
                        else:

                            # search and rescue mode is reverse so need to continue searching
                            if self.__search_and_rescue_ending_method == SearchAndRescueEndingType.reverse:

                                # crop initial path waypoints from the search area navigation waypoint list
                                if self.search_cycle_count == 0:
                                    self.waypoints = self.waypoints[self.__fallback_waypoint_count:]

                                # another search area cycle completed
                                self.search_cycle_count += 1

                                # reverse the target waypoint list
                                self.waypoints.reverse()

                                # reset target location count
                                self.waypoint_next_count = 0

                        # assign next target location
                        self.next_waypoint = self.waypoints[self.waypoint_next_count]

                        # assign goto altitude method while navigating to next target location
                        self.__goto_altitude_method = self.next_waypoint.goto_altitude_method

                # in this mode vehicle always has to turn to target
                self.heading = self.waypoint_heading

                # calculate x axis speed
                if self.waypoint_approached:
                    self.x_speed = clamp(self.waypoint_distance_2d * 0.25, configuration.minimum_x_speed,
                                         configuration.maximum_x_speed)
                else:
                    self.x_speed = configuration.maximum_x_speed

                # goto method first needs to ascend or descend to target altitude
                if self.__goto_altitude_method == GotoAltitudeClearingType.target_altitude:

                    # we still need to reach to target altitude
                    if not self.target_altitude_reached and not self.guide.noise:
                        # set commands using exposed properties
                        self.x_speed = 0.0
                        self.y_speed = 0.0

                    # desired altitude
                    self.altitude = self.next_waypoint.z

                # goto method maintains altitude and ascend or descend when reached target position in 2D
                if self.__goto_altitude_method == GotoAltitudeClearingType.maintain_altitude:

                    # reached target position, vehicle ascend or descend from maintained altitude to target altitude
                    if self.waypoint_reached_2d:

                        # set commands using exposed properties
                        self.altitude = self.next_waypoint.z

                    # not reached target position, maintain altitude
                    else:

                        # set commands using exposed properties
                        self.altitude = self.__goto_maintaining_altitude

                # reach to target altitude while navigating towards it
                if self.__goto_altitude_method == GotoAltitudeClearingType.ramp_altitude:
                    # set commands using exposed properties
                    self.altitude = self.next_waypoint.z

                # goto method first needs to ascend or descend to telecommunication altitude
                if self.__goto_altitude_method == GotoAltitudeClearingType.telecommunication_altitude:
                    raise NotImplementedError

            # clear collision related lists and flags
            self.__collision_companions = []

            # get all the companions one by one
            for companion in self.companion_vehicles:

                # check if there is a noise
                if self.guide.noise:

                    # calculate horizontal distance between companion and vehicle
                    horizontal_distance = distance_2d(Point2D(self.x, self.y), Point2D(companion.x, companion.y))

                    # check if they have enough horizontal separations
                    if horizontal_distance < configuration.default_collision_avoidance_horizontal_separation:

                        # add possible collision companion to list
                        if companion.number not in self.__collision_companions:
                            self.__collision_companions.append(companion.number)

                # there is no noise so altitudes can be used
                else:

                    # first check if they have enough vertical separation
                    if abs(self.z - companion.z) < configuration.default_collision_avoidance_vertical_separation:

                        # calculate horizontal distance between companion and vehicle
                        horizontal_distance = distance_2d(Point2D(self.x, self.y), Point2D(companion.x, companion.y))

                        # check if they have enough horizontal separations
                        if horizontal_distance < configuration.default_collision_avoidance_horizontal_separation:

                            # add possible collision companion to list
                            if companion.number not in self.__collision_companions:
                                self.__collision_companions.append(companion.number)

            # we have possible collision
            if self.possible_collision:

                # create local collision list
                collision_list = self.__collision_companions[:] + [self.number]

                # sort local collision list
                collision_list.sort()

                # get index
                index = collision_list.index(self.number)

                # calculate and set collision avoidance related commands
                self.__collision_avoidance_altitude_offset = index * 0.0

            # we do not have possible collision
            else:

                # revert avoidance related commands
                self.__collision_avoidance_altitude_offset = 0.0

            # check that if rescue logic enabled
            if self.enable_rescue:

                # help related logic
                if self.__goto_casualty:
                    # find closest casualty
                    sorted_casualties = self.casualties_injured[:]
                    sorted_casualties.sort(key=lambda casualty: distance_2d(Point2D(casualty.x, casualty.y),
                                                                            Point2D(self.x, self.y)))

                    # closest casualty will be rescued
                    self.__casualty_rescue = sorted_casualties[0]
                    target_locations = [Waypoint(x=self.__casualty_rescue.x,
                                                 y=self.__casualty_rescue.y,
                                                 z=self.scenario.extraction_rescue_altitude - 1,
                                                 goto_altitude_method=GotoAltitudeClearingType.maintain_altitude)] * 2

                    # call search and rescue
                    self.search_and_rescue(target_locations=target_locations, end=SearchAndRescueEndingType.wait)

                    # friendly reminder
                    print("Extracting from:", self.__casualty_rescue.x, self.__casualty_rescue.y,
                          self.scenario.extraction_rescue_altitude)

                # we need to go to a hospital
                if self.__goto_hospital:
                    # find closest hospital
                    sorted_hospitals = self.hospitals[:]
                    sorted_hospitals.sort(key=lambda hospital: distance_2d(Point2D(hospital.x, hospital.y),
                                                                           Point2D(self.x, self.y)))

                    # closest hospital will be selected
                    self.__hospital_closest = sorted_hospitals[0]
                    target_locations = [Waypoint(x=self.__hospital_closest.x,
                                                 y=self.__hospital_closest.y,
                                                 z=self.scenario.extraction_release_altitude - 1,
                                                 goto_altitude_method=GotoAltitudeClearingType.maintain_altitude)] * 2

                    # call search and rescue
                    self.search_and_rescue(target_locations=target_locations, end=SearchAndRescueEndingType.wait)

                    # friendly reminder
                    print("Extracting to:", self.__hospital_closest.x, self.__hospital_closest.y,
                          self.scenario.extraction_release_altitude)

                # if we have successfully extracted a casualty
                if self.casualty_extracted_count > self.__last_casualty_extracted_count:
                    # reset casualty who will be rescued
                    self.__casualty_rescue = None

                    # reset closest hospital
                    self.__hospital_closest = None

                    # update last casualty extracted count
                    self.__last_casualty_extracted_count = self.casualty_extracted_count

            # cooling down
            time.sleep(0.0001)

    # takeoff mode
    def takeoff(self, takeoff_altitude=0.0):
        """
        Takeoff command

        :param takeoff_altitude: Desired altitude
        :type takeoff_altitude: float
        """

        # only accept positive altitude value for takeoff procedure
        if takeoff_altitude > 0.0:

            # target location same x and y coordinate but different altitude
            self.next_waypoint = Waypoint(self.x, self.y, takeoff_altitude)

            # friendly info
            print("Vehicle will takeoff to: " + str(self.__command_altitude))

            # set properties and flags
            self.__mode_previous = self.mode
            self.mode = FlightMode.takeoff

        # unaccepted takeoff altitude
        else:
            print("Vehicle takeoff failed")

    # goto mode
    def goto(self, target_location=Waypoint(), goto_altitude_method=configuration.goto_altitude_method):
        """
        Goto command

        :param target_location: Target location
        :type target_location: :class:`testucusu.object.Waypoint`
        :param goto_altitude_method: Altitude method
        :type goto_altitude_method: :class:`testucusu.object.GotoAltitudeClearingType`
        """

        # make sure that target location is not empty and it is valid
        if not isinstance(target_location, Waypoint):

            # wait at the location that goto command received
            self.next_waypoint = Waypoint(self.x, self.y, self.z)

            # friendly warning
            print("Invalid target location, falling back to current location")

        # target location is not empty
        else:

            # target location does not have altitude
            if target_location.z == -1:

                # go to the location with maintaining current altitude
                self.next_waypoint = Waypoint(target_location.x, target_location.y, self.z)

                # friendly reminder
                print("Going to 2D target location with current altitude")

            # target location has everything we need
            else:

                # go to the given location
                self.next_waypoint = Waypoint(target_location.x, target_location.y, target_location.z)

                # friendly reminder
                print("Going to 3D target location")

        # make sure that goto method is valid
        if isinstance(goto_altitude_method, GotoAltitudeClearingType):

            # set altitude method
            self.__goto_altitude_method = goto_altitude_method

            # friendly reminder
            print("Goto altitude mode: " + str(self.__goto_altitude_method.name))

        # goto method is invalid
        else:

            # friendly reminder
            print("Invalid goto altitude mode, falling back to: " + str(self.__goto_altitude_method.name))

        # set flight mode and goto method
        self.__goto_maintaining_altitude = self.z
        self.__mode_previous = self.mode
        self.mode = FlightMode.goto

    # search mode
    def search_and_rescue(self, target_locations=None, end=SearchAndRescueEndingType.reverse):

        # search failsafe
        search_failsafe = False

        # check if we got target locations and list is not none
        if type(target_locations) is list or type(target_locations) is tuple:

            # check if all target locations is an instance of 3d point
            for location in target_locations:

                # if it is not Waypoint instance, set failsafe
                if not isinstance(location, Waypoint):
                    search_failsafe = True

                # make sure that goto method is valid
                if not isinstance(location.goto_altitude_method, GotoAltitudeClearingType):
                    search_failsafe = True

        # target_location is not list or tuple, there is a failsafe
        else:
            search_failsafe = True

        # there is an error in somewhere
        if search_failsafe:

            # friendly reminder
            print("There is an error using search mode")

        # no error in search mode request
        else:

            # get target locations
            self.waypoints = target_locations

            # create path from current location to first target location of the search area
            waypoints, path = path_creator(first_point=shp.Point(self.x, self.y),
                                           second_point=shp.Point(target_locations[0].x, target_locations[0].y),
                                           interpolation_distance=configuration.default_interpolation_distance)

            # create avoidance related objects
            no_fly_zones, no_fly_zone_points = no_fly_zone_creator(no_fly_zone_circles=None,
                                                                   no_fly_zone_polygons=self.scenario.denied_zones,
                                                                   interpolation_distance=configuration.default_interpolation_distance,
                                                                   buffer_zone_width=configuration.default_buffer_zone_width)

            # avoid from denied zones
            waypoints = avoidance(waypoints=waypoints,
                                  no_fly_zones=no_fly_zones,
                                  no_fly_zone_points=no_fly_zone_points)

            # create waypoint object list
            target_locations = []
            for waypoint in waypoints:
                target_locations.append(Waypoint(x=waypoint.x,
                                                 y=waypoint.y,
                                                 z=configuration.default_goto_altitude + self.number * configuration.default_collision_avoidance_vertical_separation,
                                                 goto_altitude_method=GotoAltitudeClearingType.target_altitude))

            # get target locations
            self.waypoints = target_locations + self.waypoints

            # set flight mode and goto method
            self.__goto_maintaining_altitude = self.z
            self.waypoint_next_count = 0
            self.__mode_previous = self.mode
            self.next_waypoint = self.waypoints[0]
            self.__goto_altitude_method = self.waypoints[0].goto_altitude_method
            self.mode = FlightMode.search_and_rescue
            self.search_cycle_count = 0
            self.__fallback_waypoint_count = len(target_locations)
            self.__search_and_rescue_ending_method = end
