import shapely.geometry as shp
import shapely.ops as ops
from shapely import speedups as spd
from testucusu.util import destination


def speedups(permission=True):
    """
    Enables or disables speedups

    :param permission: Permission is granted or not for speedups
    :type permission: bool
    """

    # enable speedups
    if not spd.enabled:
        if permission:
            if spd.available:
                spd.enable()
        else:
            spd.disable()
        if spd.enabled:
            print("shapely speedups enabled")
        else:
            print("shapely speedups disabled")


def distance(first_point, second_point):
    """
    Calculates distance between two waypoints

    :param first_point: First waypoint
    :type first_point: :class:`shapely.geometry.Point`
    :param second_point: Second waypoint
    :type second_point: :class:`shapely.geometry.Point`
    :return: Distance between waypoints
    :rtype: float
    """

    # return to calculated distance
    return first_point.distance(second_point)


def waypoint_creator(area_points, interpolation_distance):
    """
    Calculates waypoints for navigation

    :param area_points: List of corners of search area
    :type area_points: list of :class:`shapely.geometry.Point`
    :param interpolation_distance: Distance between waypoints
    :type interpolation_distance: float
    :return: Navigation waypoints
    :rtype: list of :class:`shapely.geometry.Point`
    """

    # create search area boundaries
    area_boundaries = shp.LinearRing(area_points)

    # initiate pseudo polygon that will be eroded
    rolling_area_polygon = shp.Polygon(area_boundaries)

    # initiate interpolated boundaries point list
    interpolation_points = []

    # calculate way points
    while not rolling_area_polygon.is_empty:

        # if erosion did not create multiple polygon objects
        if rolling_area_polygon.geom_type != "MultiPolygon":

            # calculate the circumference of the polygon
            boundaries_length = rolling_area_polygon.exterior.length

            # calculate the allowed interpolation point amount
            allowed_interpolation_point_count = round(boundaries_length / interpolation_distance)

            # create interpolation points
            for j in range(allowed_interpolation_point_count):

                # create interpolation point
                interpolation_point = rolling_area_polygon.exterior.interpolate(j * interpolation_distance)

                # interpolation point has at least one point before
                if interpolation_points > []:

                    # if distance between current waypoint and last waypoint on the list is far enough
                    if distance(interpolation_points[-1], interpolation_point) >= 2 * interpolation_distance:

                        # we need to interpolate this distance
                        waypoints, _ = path_creator(interpolation_points[-1], interpolation_point,
                                                    interpolation_distance)

                        # remove first and last waypoint because we already have them
                        waypoints = waypoints[1:-1]

                        # append them to list
                        if len(waypoints) > 0:
                            for waypoint in waypoints:
                                interpolation_points.append(waypoint)

                # append it to list
                interpolation_points.append(interpolation_point)

        # erosion created multiple polygon objects
        else:

            # for each eroded area do interpolation operation
            for area in rolling_area_polygon:

                # calculate the circumference of the polygon
                boundaries_length = area.exterior.length

                # calculate the allowed interpolation point amount
                allowed_interpolation_point_count = round(boundaries_length / interpolation_distance)

                # create interpolation points
                for j in range(allowed_interpolation_point_count):

                    # create interpolation point
                    interpolation_point = area.exterior.interpolate(j * interpolation_distance)

                    # interpolation point has at least one point before
                    if interpolation_points > []:

                        # if distance between current waypoint and last waypoint on the list is far enough
                        if distance(interpolation_points[-1], interpolation_point) >= 2 * interpolation_distance:

                            # we need to interpolate this distance
                            waypoints, _ = path_creator(interpolation_points[-1], interpolation_point,
                                                        interpolation_distance)

                            # remove first and last waypoint because we already have them
                            waypoints = waypoints[1:-1]

                            # append them to list
                            if len(waypoints) > 0:
                                for waypoint in waypoints:
                                    interpolation_points.append(waypoint)

                    # append it to list
                    interpolation_points.append(interpolation_point)

        # erode the pseudo polygon with amount of interpolation distance
        rolling_area_polygon = rolling_area_polygon.buffer(- interpolation_distance)

    # return list of calculated waypoints
    return interpolation_points


def no_fly_zone_creator(no_fly_zone_circles, no_fly_zone_polygons, interpolation_distance, buffer_zone_width):
    """
    Calculates no-fly zones and no-fly zone points

    :param no_fly_zone_circles: No-fly zone circle parameters
    :type no_fly_zone_circles: list
    :param no_fly_zone_polygons: No-fly zone polygon parameters
    :type no_fly_zone_polygons: list
    :param interpolation_distance: Distance between waypoints
    :type interpolation_distance: float
    :param buffer_zone_width: Buffer of no-fly zones
    :type buffer_zone_width: float
    :return: Tuple of no-fly zones list and no-fly zone points list
    :rtype: (:class:`shapely.geometry.MultiPolygon`, list of (list of :class:`shapely.geometry.Point`))
    """

    # create no-fly zone objects
    no_fly_zones = []

    if no_fly_zone_circles is not None:
        for zone in no_fly_zone_circles:
            no_fly_zones.append(shp.Point(zone[0]).buffer(zone[1] + buffer_zone_width))

    if no_fly_zone_polygons is not None:
        for zone in no_fly_zone_polygons:
            boundaries = shp.LinearRing(zone)
            no_fly_zones.append(shp.Polygon(boundaries).buffer(buffer_zone_width))

    no_fly_zones = ops.unary_union(no_fly_zones)
    if no_fly_zones.geom_type == "Polygon":
        no_fly_zone_areas = [no_fly_zones]
    else:
        no_fly_zone_areas = no_fly_zones

    # initiate no-fly zone interpolated point list
    no_fly_zone_points = []

    # for each eroded area do interpolation operation
    for area in no_fly_zone_areas:

        # calculate the circumference of the polygon
        boundaries_length = area.length

        # calculate the allowed interpolation point amount
        allowed_interpolation_point_count = round(boundaries_length / interpolation_distance)

        # create interpolation points
        no_fly_zone_points.append([])
        for j in range(allowed_interpolation_point_count):
            no_fly_zone_points[-1].append(area.exterior.interpolate(j * interpolation_distance))

    # return tuple of no-fly zones and no-fly zone waypoints list
    return no_fly_zones, no_fly_zone_points


def closest_point(my_list, my_point):
    """
    Calculates the closest point in the list to the given point

    :param my_list: List of points that the closest point will be found inside
    :type my_list: list of :class:`shapely.geometry.Point`
    :param my_point: Given point that will be used to find closest point to it
    :type my_point: :class:`shapely.geometry.Point`
    :return: Closest point to given point
    :rtype: :class:`shapely.geometry.Point`
    """

    # calculate and return to the closest point
    return next((my_closest_point for my_closest_point in my_list if my_closest_point.distance(my_point) ==
                 min([distance(my_closest_point, my_point) for my_closest_point in my_list])), None)


def path_creator(first_point, second_point, interpolation_distance):
    """
    Calculates navigation waypoints from starting point to destination point and navigation path

    :param first_point: First waypoint
    :type first_point: :class:`shapely.geometry.Point`
    :param second_point: Second waypoint
    :type second_point: :class:`shapely.geometry.Point`
    :param interpolation_distance: Distance between waypoints
    :type interpolation_distance: float
    :return: Navigation waypoints and navigation path
    :rtype: (list of :class:`shapely.geometry.Point`, :class:`shapely.geometry.LineString`)
    """

    # create path
    path = shp.LineString([first_point, second_point])

    # calculate waypoints
    waypoints = []

    # if length of path is less than interpolation distance do not do anything
    if first_point.distance(second_point) <= interpolation_distance:

        # append first point and second point to the waypoint list
        waypoints.append(first_point)
        waypoints.append(second_point)

    # length of path is greater than interpolation distance so calculate interpolation points
    else:

        # calculate the length of path
        boundaries_length = path.length

        # calculate the allowed interpolation point amount
        allowed_interpolation_point_count = round(boundaries_length / interpolation_distance)

        # create interpolation points
        for j in range(allowed_interpolation_point_count):
            waypoints.append(path.interpolate(j * interpolation_distance))

        # append destination waypoint for safety
        waypoints.append(second_point)

    # return to created path and calculated path waypoints
    return waypoints, path


def avoidance(waypoints, no_fly_zones, no_fly_zone_points):
    """
    Detects no-fly zone breaches and avoids from no-fly zones by inserting escape waypoints

    If starting waypoint inside a no-fly zone, it immediately gets out of the area

    If ending waypoint inside a no-fly zone, it goes to that point no matter what

    It is wise to give to the algorithm a waypoint list so that starting and ending waypoints not inside a no-fly zone

    :param waypoints: Waypoint list of the navigation
    :type waypoints: list of :class:`shapely.geometry.Point`
    :param no_fly_zones: No-fly zone list to be avoided
    :type no_fly_zones: :class:`shapely.geometry.MultiPolygon`
    :param no_fly_zone_points: No-fly zone point list to be inserted when breach detected
    :type no_fly_zone_points: list of (list of :class:`shapely.geometry.Point`)
    :return: Waypoint list that avoided from no-fly zones
    :rtype: list of :class:`shapely.geometry.Point`
    """

    # initiate variables
    detection_indexes = []
    starting_point_index = 0
    ending_point_index = 0
    breached_zone_index = 0

    # avoid single denied zones
    if no_fly_zones.geom_type == "Polygon":
        no_fly_zone_areas = [no_fly_zones]
    else:
        no_fly_zone_areas = no_fly_zones

    # for each waypoint, give them a number and
    for i, waypoint in enumerate(waypoints):

        # if waypoint is in the one of the restricted areas
        if waypoint.within(no_fly_zones):

            # find breached zone
            for j, zone in enumerate(no_fly_zone_areas):
                if waypoint.within(zone):
                    breached_zone_index = j

            # update point index before breach
            if ending_point_index == 0 and starting_point_index == 0:
                starting_point_index = i - 1

        # waypoint is not in the one of the restricted areas
        else:

            # add this good point to the new waypoint list
            # new_waypoints.append(waypoint)

            # update point index after breach
            if starting_point_index != 0:
                ending_point_index = i

        # breached in and get out of one of the no-fly zone areas so report it
        if starting_point_index != 0 and ending_point_index != 0:
            detection_indexes.append(((starting_point_index, ending_point_index), breached_zone_index))
            starting_point_index = 0
            ending_point_index = 0

    # initiate loop variables
    avoidance_paths = []
    normal_paths = []

    # for each detected breach
    for i, ((starting_point_index, ending_point_index), breached_zone_index) in enumerate(detection_indexes):

        # get starting and ending points of the breach from waypoints
        starting_point = waypoints[starting_point_index]
        ending_point = waypoints[ending_point_index]

        # get points of breached no-fly zone
        breached_zone_points = no_fly_zone_points[breached_zone_index]

        # get closest points to starting and ending points of the breach from waypoints on the no-fly zone points
        breached_zone_starting_point = closest_point(breached_zone_points, starting_point)
        breached_zone_ending_point = closest_point(breached_zone_points, ending_point)

        # get indexes of the closest points
        breached_zone_starting_point_index = breached_zone_points.index(breached_zone_starting_point)
        breached_zone_ending_point_index = breached_zone_points.index(breached_zone_ending_point)

        # arrange starting and ending index
        path_start_index = min(breached_zone_starting_point_index, breached_zone_ending_point_index)
        path_end_index = max(breached_zone_starting_point_index, breached_zone_ending_point_index)

        # get path lengths between closest points
        inner_path_length = path_end_index - path_start_index
        outer_path_length = len(breached_zone_points) - inner_path_length

        # if length of inner path is less than length of outer path
        if inner_path_length <= outer_path_length:

            # create unordered path
            path = breached_zone_points[path_start_index: path_end_index]

        # if length of outer path is less than length of inner path
        else:

            # create unordered path
            path = breached_zone_points[path_end_index:] + breached_zone_points[:path_start_index]

        # order the path to ensure continuity of the waypoints for navigation
        if path > []:
            if path[0] != closest_point(path, starting_point):
                path.reverse()

        # add escape waypoint sublist chunk to avoidance path list
        avoidance_paths.append(path)

        # add first normal path waypoint sublist chunk to normal waypoint list
        if i == 0:
            normal_paths.append(waypoints[:starting_point_index + 1])

        # add rest of the normal path waypoint sublist chunk to normal waypoint list
        else:
            normal_paths.append(waypoints[detection_indexes[i - 1][0][1]:starting_point_index + 1])

    # add normal paths and avoidance paths repetitively
    new_waypoints = []
    for i, normal_path in enumerate(normal_paths):

        # add normal path waypoints
        for waypoint in normal_path:
            new_waypoints.append(waypoint)

        # add avoidance waypoints
        for waypoint in avoidance_paths[i]:
            new_waypoints.append(waypoint)

    # add normal path waypoints
    if detection_indexes > []:
        for waypoint in waypoints[detection_indexes[-1][0][1]:]:
            new_waypoints.append(waypoint)
    else:
        new_waypoints = waypoints

    # return to new waypoints list
    return new_waypoints


def area_divider(uav_number=0, area_points=None, interpolation_distance=0.0, buffer_zone_width=0.0):
    """
    This function divides search area to sub search areas and assigns them to corresponding UAVs

    :param uav_number: Number of UAVs that will be deployed
    :type uav_number: int
    :param area_points: Corner point of the search area provided
    :type area_points: list of (float, float)
    :param interpolation_distance: Distance between consecutive waypoints
    :type interpolation_distance: float
    :param buffer_zone_width: Buffer zone width of the each erosion operation
    :type buffer_zone_width: float
    :return: Waypoint lists those are assigned to individual UAVs
    :rtype: list of (list of :class:`shapely.geometry.Point`)
    """

    # create area polygon using corners
    area_polygon = shp.Polygon(area_points)

    # calculate area of the polygon
    area = area_polygon.area

    # calculate maximum area that each UAV should cover
    maximum_responsible_area = area / uav_number

    # find center of mass position of the polygon
    area_center = area_polygon.centroid

    # calculate the length of the line that will divide the area into individual UAV search areas
    furthest_distance = area_center.hausdorff_distance(area_polygon) * 7

    # variables those used to hold valuable information about area divider algorithm
    cutter_polygons = []
    divided_polygons = []
    angle = 0

    # main area divider procedures that could divide search area into n > 2 individual UAV search areas
    if uav_number > 2:

        # run this loop n - 1 times (all UAVs except the last one)
        for i in range(0, uav_number - 1):

            # scan search area
            for j in range(0, 360):

                # ending point of the first vector
                vector_point_start = shp.Point(destination(area_center.coords[0], angle, furthest_distance))

                # ending point of the second vector
                vector_point_end = shp.Point(destination(area_center.coords[0], angle + j, furthest_distance))

                # create cutter polygon using area centroid and endpoints of the starting and ending vectors
                cutter_polygon = shp.Polygon((area_center, vector_point_start, vector_point_end))

                # calculate area of intersection of area and cutter polygons which is individual responsibility area
                responsible_area = area_polygon.intersection(cutter_polygon)

                # if the coverage of the current UAV is enough, break, save it to cutter polygons and go next
                if responsible_area.area > maximum_responsible_area:
                    angle = angle + j
                    cutter_polygons.append(cutter_polygon)
                    break

        # below lines used for the cutter polygon of the last UAV

        # ending point of the first vector
        vector_point_start = shp.Point(destination(area_center.coords[0], angle, furthest_distance))

        # ending point of the second vector which is the starting point of the above loop
        vector_point_end = shp.Point(destination(area_center.coords[0], 0, furthest_distance))

        # create cutter polygon using area centroid and endpoints of the starting and ending vectors
        cutter_polygon = shp.Polygon((area_center, vector_point_start, vector_point_end))

        # save cutter polygon
        cutter_polygons.append(cutter_polygon)

        # for each cutter polygon in the list
        for cutter_polygon in cutter_polygons:

            # calculate intersection of the cutter and area polygon, then erode it with buffer zone width
            intersection = area_polygon.intersection(cutter_polygon).buffer(-buffer_zone_width)

            # if result is polygon, then is responsibility area of the current UAV
            if intersection.geom_type == "Polygon":
                divided_polygons.append(intersection)

            # if result is multi polygon, they needed to be assigned to individual UAV separately
            elif intersection.geom_type == "MultiPolygon":
                divided_polygons.append([])
                for polygon in intersection:
                    divided_polygons[-1].append(polygon)

            # this exception will never raise
            else:
                raise NotImplementedError("Serious error on area divider algorithm")

    # if n < 2, area divider needs to follow different procedures
    elif uav_number == 2:

        # scan search area
        for j in range(0, 360):

            # ending point of the first vector
            vector_point_start = shp.Point(destination(area_center.coords[0], j, furthest_distance))

            # ending point of the second vector
            vector_point_end = shp.Point(destination(area_center.coords[0], j + 180, furthest_distance))

            # create line string using two vector endpoints
            line = shp.LineString((vector_point_start, vector_point_end))

            # create polygon from this line
            cutter_polygon = line.buffer(buffer_zone_width)

            # create responsibility areas of both UAVs
            responsible_areas = area_polygon.difference(cutter_polygon)

            # if difference between two responsibility areas is less than %5 of the main area
            if abs(responsible_areas[0].area - responsible_areas[1].area) < area * 0.05:
                # assign responsibility areas
                divided_polygons.append(responsible_areas[0])
                divided_polygons.append(responsible_areas[1])

                # we are done
                break

    # no need to divide the search area, just erode it
    elif uav_number == 1:
        divided_polygons.append(area_polygon.buffer(-buffer_zone_width))

    # this exception will never raise
    else:
        raise NotImplementedError("Serious error on area divider algorithm")

    # this list holds waypoints assigned by UAV index
    waypoints = []

    # loop in divided polygons
    for polygon in divided_polygons:

        # if current UAV has more than one search area assigned
        if type(polygon) == list:

            # initiate a new waypoint list
            sub_waypoints_list = []

            # for each responsible search areas
            for i, sub in enumerate(polygon):

                # create navigation waypoint inside current search area
                sub_waypoints_list.append(waypoint_creator(sub.exterior.coords, interpolation_distance))

                # if this search area is not the first responsibility area of the current UAV
                if i > 0:
                    # calculate nearest points between now and before responsibility area of the current UAV
                    nearest_points = ops.nearest_points(polygon[i - 1], polygon[i])

                    # create a path from last point of before area to nearest point on before area to current area
                    sub_waypoints_list.insert(-1, path_creator(sub_waypoints_list[i - 1][-1], nearest_points[0],
                                                               interpolation_distance)[0])

                    # create a path from nearest point on before area to current area nearest point on current area
                    sub_waypoints_list.insert(-1, path_creator(nearest_points[0], nearest_points[0],
                                                               interpolation_distance)[1])

                    # create a path from current area nearest point on current area to first point of current area
                    sub_waypoints_list.insert(-1, path_creator(nearest_points[1], sub_waypoints_list[-1][0],
                                                               interpolation_distance)[0])

            # accumulate waypoints on the new list to waypoint list without extra list dimension
            waypoints.append([])
            for sub_waypoints in sub_waypoints_list:
                for sub_waypoint in sub_waypoints:
                    waypoints[-1].append(sub_waypoint)

        # UAV has one search area assigned
        else:

            # calculate and deploy waypoints to current UAV
            waypoints.append(waypoint_creator(polygon.exterior.coords, interpolation_distance))

    # return to waypoints and responsibility areas since they might be used
    return waypoints, divided_polygons
