import enum


# environment level class
@enum.unique
class Environment(enum.Enum):
    """
    This is the environment level abbreviations class
    """

    not_defined = enum.auto()
    """
    Simulation environment not defined
    """

    development = enum.auto()
    """
    Source code is under development
    """

    debugging = enum.auto()
    """
    Source code is debugging
    """

    test = enum.auto()
    """
    Source code is testing in undefined server on human supervision
    """

    test_home = enum.auto()
    """
    Source code is testing in home server on human supervision
    """

    test_university = enum.auto()
    """
    Source code is testing in university server on human supervision
    """

    test_azure = enum.auto()
    """
    Source code is testing in azure server on human supervision
    """

    test_droplet = enum.auto()
    """
    Source code is testing in droplet server on human supervision
    """

    auto_test = enum.auto()
    """
    Source code testing itself in undefined server without human supervision
    """

    auto_test_home = enum.auto()
    """
    Source code testing itself in home server without human supervision
    """

    auto_test_university = enum.auto()
    """
    Source code testing itself in university server without human supervision
    """

    auto_test_azure = enum.auto()
    """
    Source code testing itself in azure server without human supervision
    """

    auto_test_droplet = enum.auto()
    """
    Source code testing itself in droplet server without human supervision
    """

    sandbox = enum.auto()
    """
    Source code easy use enabled in local computer
    """

    production = enum.auto()
    """
    Source code is ready to use for production
    
    This source code contains production level fully autonomous swarm intelligence
    
    Info, warning and error messages and logs enabled
    """

    competition = enum.auto()
    """
    Production level source code with competition requirements
    
    Messages disabled and logs enabled
    """


# flight modes class
@enum.unique
class FlightMode(enum.Enum):
    """
    This is the flight mode abbreviations class
    """

    not_defined = enum.auto()
    """
    It means that vehicle is in a not defined flight mode

    Vehicles only in this mode when the :class:`testucusu.uav.UAV` class is initialized
    """

    initialize = enum.auto()
    """
    It means that vehicle is in initializing mode
        
    In this mode vehicle is making some preflight checklist satisfaction procedures and do some navigation calculations
    
    This mode can be set from user only
    
    Preflight calculations thread wait vehicles to be in this mode
    """

    ready_to_fly = enum.auto()
    """
    It means that vehicle is at the ready to fly state
    
    This will fired up when preflight calculations thread done all of the navigation calculations
    
    Vehicle can only be takeoff if it is in this state and also if it is armed
    """

    takeoff = enum.auto()
    """
    In this mode vehicle climbs to given altitude if it is armed
    
    Can be set from user when vehicle is in :class:`testucusu.abbreviations.FlightMode.ready_to_fly` flight mode
    """

    land = enum.auto()
    """
    In this mode vehicle lands to its current location and vehicle disarms itself
    """

    return_to_home = enum.auto()
    """
    In this mode vehicle goes to home location with maintaining a safe altitude (typically at the telecommunication 
    service altitude range for collecting some points) and avoiding obstacles (hopefully)
    
    When vehicle reaches to home location, it immediately changes it's flight mode to 
    :class:`testucusu.abbreviations.FlightMode.land` and disarms itself
    """

    goto = enum.auto()
    """
    In this mode vehicle try to go to the given location at the given altitude
    
    Vehicle calculates shortest path and avoids obstacles if there are any
    """

    goto_relaxed = enum.auto()
    """
    In this mode vehicle try to go to the given location at the given altitude

    Vehicle calculates shortest path and relaxes the navigation path by optimizing telecommunication service and 
    avoids obstacles if there are any
    """

    position_hold = enum.auto()
    """
    In this mode vehicle try to maintain it's current 3D location
    
    When vehicle reaches to a location within the error margin using :class:`testucusu.abbreviations.FlightMode.goto`, 
    this mode can be used to accurately satisfy the desired location
    
    This mode is slower than :class:`testucusu.abbreviations.FlightMode.goto` because main goal is precision

    """

    swarm = enum.auto()
    """
    This mode used when vehicles follow the leader to the search area
    
    Vehicle calculates the 3D location to be and use :class:`testucusu.abbreviations.FlightMode.goto` procedures if the
    location is too far (may be create a navigation path)
    
    If the desired 3D location is within the error margin then the vehicle switches to 
    :class:`testucusu.abbreviations.FlightMode.position_hold` procedures and updates the 3D position to be hold 
    continuously
    
    If vehicle could not be able to keep up this movement with :class:`testucusu.abbreviations.FlightMode.position_hold`
    procedures then switches back to :class:`testucusu.abbreviations.FlightMode.goto` procedures and this loop continue 
    till the dispatch
    """

    search_and_rescue = enum.auto()
    """
    In this mode vehicle continuously feed :class:`testucusu.abbreviations.FlightMode.goto` procedures with waypoints
    created in the :class:`testucusu.abbreviations.FlightMode.initialize` procedures by preflight calculations thread
    
    At the end of the waypoint list, the list will be reversed and vehicle continues to search

    This mode can be interrupted when vehicle detects a personal to be rescued
    """


# vehicle status class
@enum.unique
class VehicleStatus(enum.Enum):
    """
    This is the vehicle status abbreviations class
    """

    rescue_to_personal = enum.auto()
    """
    Vehicle going to injured casualty
    """

    rescue_initial_descend = enum.auto()
    """
    Vehicle descending to injured casualty 
    """

    rescue_initial_ascend = enum.auto()
    """
    Vehicle got casualty and ascending
    """

    rescue_to_hospital = enum.auto()
    """
    Vehicle is going to closest hospital
    """

    rescue_final_descend = enum.auto()
    """
    Vehicle reached hospital location and descending
    """

    rescue_final_ascend = enum.auto()
    """
    Vehicle extracted casualty and ascending
    """

    rescuing_casualty = enum.auto()
    """
    Vehicle waiting on top of the casualty to get him/her
    """

    extracting_casualty = enum.auto()
    """
    Vehicle waiting on top of the hospital to release him/her
    """

    collision_avoidance_static_level = enum.auto()
    """
    Vehicle avoiding collision with leveling up
    """

    collision_avoidance_static_climb = enum.auto()
    """
    Vehicle avoiding collision with climbing
    """

    collision_avoidance_static_descent = enum.auto()
    """
    Vehicle avoiding collision with descending
    """

    collision_avoidance_static_maneuver_left = enum.auto()
    """
    Vehicle avoiding collision with turning left
    """

    collision_avoidance_static_maneuver_right = enum.auto()
    """
    Vehicle avoiding collision with turning right
    """

    collision_avoidance_dynamic_level = enum.auto()
    """
    Vehicle avoiding collision with other vehicle with leveling up
    """

    collision_avoidance_dynamic_climb = enum.auto()
    """
    Vehicle avoiding collision with other vehicle with climbing
    """

    collision_avoidance_dynamic_descent = enum.auto()
    """
    Vehicle avoiding collision with other vehicle with descending
    """

    collision_avoidance_dynamic_maneuver_left = enum.auto()
    """
    Vehicle avoiding collision with other vehicle with turning left
    """

    collision_avoidance_dynamic_maneuver_right = enum.auto()
    """
    Vehicle avoiding collision with other vehicle with turning right
    """


# duties class
@enum.unique
class Duty(enum.Enum):
    """
    This is the duty abbreviations class
    """

    default = "D"
    """
    Vehicle is doing anything but serving telecommunication or rescuing casualty
    """

    telecommunication = "T"
    """
    Vehicle is serving telecommunication
    """

    rescue = "K"
    """
    Vehicle is rescuing casualty
    """

    not_defined = "X"
    """
    It means that vehicle is in a not defined duty

    Vehicles only in this duty when the :class:`testucusu.uav.UAV` class is initialized
    """


# formation type class
@enum.unique
class FormationType(enum.Enum):
    """
    This is the formation type class and holds formation type that the swarm has to follow
    """

    arrow = "arrow"
    """
    Simulation wants vehicle swarm to be in arrow formation
    """

    prism = "prism"
    """
    Simulation wants vehicle swarm to be in rectangular prism formation
    """

    not_defined = "unknown"
    """
    Undefined formation type
    """


# asset type class
@enum.unique
class AssetType(enum.Enum):
    """
    This is the asset type class and holds types of assets in the simulation world
    """

    casualty_healthy = enum.auto()
    """
    It is a person and needs assistance by telecommunication service
    """

    casualty_injured = enum.auto()
    """
    It is a person and needs assistance by extraction
    """

    casualty_unknown = enum.auto()
    """
    Undefined casualty asset type
    """

    hospital_available = enum.auto()
    """
    Available hospital that can has quota to receive extracted personnel 
    """

    hospital_unavailable = enum.auto()
    """
    Unavailable hospital that can has full and can not receive any more personnel
    """

    hospital_unknown = enum.auto()
    """
    Hospital that has no information about it's quota
    """


# altitude clearing type class
@enum.unique
class AltitudeClearingType(enum.Enum):
    """
    This is the altitude clearing type class and holds types of altitude clearings
    """

    percentage = enum.auto()
    """
    Altitude reached flag cleared by error percentage
    """

    offset = enum.auto()
    """
    Altitude reached flag cleared by error offset
    """


# altitude clearing type class for goto mode
@enum.unique
class GotoAltitudeClearingType(enum.Enum):
    """
    This is the goto type class and holds types of navigation between waypoints
    """

    target_altitude = enum.auto()
    """
    Vehicle first ascend or descend to target altitude, then navigate to target position
    """

    maintain_altitude = enum.auto()
    """
    Vehicle maintains it's current altitude, navigates to target 2D position, then ascend or descend to target altitude
    """

    ramp_altitude = enum.auto()
    """
    Vehicle try to reach to target altitude while navigating to target position
    """

    telecommunication_altitude = enum.auto()
    """
    Vehicle first ascend or descend to optimized telecommunication altitude, then navigate to target position
    """


# asset type class
@enum.unique
class SearchAndRescueEndingType(enum.Enum):
    """
    This is search and rescue type class
    """

    wait = enum.auto()
    """
    Wait at the end of the waypoint list
    """

    reverse = enum.auto()
    """
    Reverse the waypoint list at the end
    """
