from testucusu.abbreviation import Environment, AltitudeClearingType, GotoAltitudeClearingType, \
    SearchAndRescueEndingType

level = Environment.not_defined
"""
Simulation environment level for self deployment
"""

altitude_clearing_method = AltitudeClearingType.percentage
"""
Altitude clearing method for altitude controller
"""

altitude_clearing_error_percentage = 2.0
"""
Altitude clearing percentage.

If 0, altitude flag always wait desired altitude, this may lead some incorrect altitude flag behavior

If 100, altitude controller immediately sets flags and not waits desired altitude

This configuration does not affect altitude controller, the controller always try to reach desired altitude, this just
allows the flags to be set
"""

altitude_clearing_error_offset = 1.0
"""
Altitude clearing offset.

If 0, altitude flag always wait desired altitude, this may lead some incorrect altitude flag behavior

If x, altitude cleared offset is set when vehicle reached desired altitude within plus or minus x
"""

goto_altitude_method = GotoAltitudeClearingType.target_altitude
"""
Default goto mode navigation method
"""

search_and_rescue_ending_method = SearchAndRescueEndingType.reverse
"""
By default, search mode ending type is reversing the waypoint list and continuing mission
"""

target_reached_distance_default = 5.0
"""
Default target reached error distance to set target reached flag

If 0, target reached flag only set if current position is target position, which is almost never and this may lead to 
incorrect target reached flag behavior

If x, target reached flag is set when target distance is less than x

This parameter used in low resolution goto mode
"""

target_approaching_distance = 50.0
"""
If target distance is less than this distance, vehicle slowing down to not overshoot the target position
"""

target_precise_distance = 20.0
"""
If target distance is less than this distance, vehicle try to approach target position is precise goto mode
"""

target_reached_distance_search = 40.0
"""
Target reached error distance to set target reached flag in search mode

If 0, target reached flag only set if current position is target position, which is almost never and this may lead to 
incorrect target reached flag behavior

If x, target reached flag is set when target distance is less than x

This parameter used in search mode
"""

default_buffer_zone_width = 40.0
"""
Default buffer zone width between sub search areas
"""

default_interpolation_distance = 40.0
"""
Default interpolation distance between navigation waypoints
"""

default_search_altitude = 50.0
"""
Default altitude used in search mode
"""

default_goto_altitude = 50.0
"""
Default altitude used in goto mode
"""

default_collision_avoidance_horizontal_separation = 150.0
"""
Default collision avoidance horizontal separation distance

If two vehicles are close to each other less than this distance, collision avoidance logic will be fire up
"""

default_collision_avoidance_vertical_separation = 15.0
"""
Default collision avoidance vertical separation altitude difference

If two vehicles are close to each other less than this altitude difference, collision avoidance logic will be fire up
"""

waypoint_heading_deceleration_bias = 5.0
"""
Waypoint heading deceleration bias is used to decelerate the vehicle if 2nd next waypoint is not in front of vehicle
"""

maximum_x_speed = 20.0
"""
Maximum lateral speed that the navigation controller could be able to give to the vehicle
"""

maximum_y_speed = 20.0
"""
Maximum longitudinal speed that the navigation controller could be able to give to the vehicle
"""

minimum_x_speed = 0.0
"""
Minimum lateral speed that the navigation controller could be able to give to the vehicle
"""

minimum_y_speed = 0.0
"""
Minimum longitudinal speed that the navigation controller could be able to give to the vehicle
"""