# -*- coding: utf-8 -*-

import os
import sys

sys.path.insert(0, os.path.abspath('extensions'))

extensions = ['sphinx.ext.autodoc']

todo_include_todos = True
templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'index'
exclude_patterns = []
add_function_parentheses = True
# add_module_names = True
# A list of ignored prefixes for module index sorting.
# modindex_common_prefix = []

project = u'suru'
copyright = u'2020, Test Ucusu'

version = '0.0.0'
release = '0.0.0'
