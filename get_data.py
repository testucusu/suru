import time
from testucusu.uav import UAV

# initialize the UAV
my_uav = UAV(number=0, name="alpha", server_address="testucusu.local")

# wait for safety
while not my_uav.safety_switch or not my_uav.scenario.received:
    time.sleep(0.0001)

print(my_uav.scenario.world_boundaries)
print(my_uav.scenario.denied_zones)
print(my_uav.scenario.special_assets)
