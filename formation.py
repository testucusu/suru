from testucusu.object import Point2D
from testucusu.util import rotate

my_rotating_point = Point2D(5.0, 0.0)
my_center = Point2D(5.0, 5.0)

my_rotated_point = rotate(rotating_point=my_rotating_point, center_point=my_center, angle=180.0)

print(my_rotated_point.x)
print(my_rotated_point.y)
