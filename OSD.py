# -*- coding: utf-8 -*-

# System libraries
import os
import math
import sys

# Custom libraries
from Chlorophyll import Chlorophyll
from Unicodes import Unicodes
from testucusu.object import Point2D, UAV, Target, Building

# Preconfigurations
sys.dont_write_bytecode = True  # Please do not create .pyc

# Set OSD resolution
max_width = 140
max_height = 45

# Main box paddings
padding_width_left = 0
padding_width_right = 38
padding_height_top = 1
padding_height_bottom = 1

# UAV configurations
num_uav = 6

# Resize console window
os.system("resize -s " + str(max_height + 1) + " " + str(max_width))
os.system("echo -ne '\e]10;#00FF00\a'")
os.system("echo -ne '\e]11;black\a'")

# Create OSD array
OSD = [x[:] for x in [[Unicodes.space] * max_width] * max_height]

# Create header
header1 = "UAV:I ASSET MAP Live Feed: CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC @ IIII"
header2 = "Traffic Status:CCCC Server Time:IIII"
header = " " + header1 + " " * (max_width - len(header1) - padding_width_right) + header2
# header = [Chlorophyll.BOLD + x + Chlorophyll.END for x in header]
OSD[0][0:len(header)] = header

# Create results
results = "TOTAL:III% (Area:III% Detection:III% Tracking:III%) Terrorists:III Buildings:III Detections:III"
# results = [Chlorophyll.BOLD + x + Chlorophyll.END for x in results]
OSD[max_height - 1][1: len(results) + 1] = results

# Create footer
footer = "Made with ♥ by MG&MG © 2019 TEST UCUSU"
OSD[max_height - 1][max_width - len(footer) + 3:] = footer

# Create OSD box
for i in range(padding_height_top, max_height - padding_height_bottom):
    OSD[i][padding_width_left] = Unicodes.vertical
    OSD[i][max_width - padding_width_right - 1] = Unicodes.vertical
OSD[padding_height_top][padding_width_left: max_width - padding_width_right] = [Unicodes.horizontal] * (
        max_width - padding_width_left - padding_width_right)
OSD[max_height - padding_height_bottom - 1][padding_width_left: max_width - padding_width_right] = [
                                                                                                       Unicodes.horizontal] * (
                                                                                                           max_width - padding_width_left - padding_width_right)
OSD[padding_height_top][padding_width_left] = Unicodes.top_left
OSD[padding_height_top][max_width - padding_width_right - 1] = Unicodes.top_right
OSD[max_height - padding_height_bottom - 1][padding_width_left] = Unicodes.bottom_left
OSD[max_height - padding_height_bottom - 1][max_width - padding_width_right - 1] = Unicodes.bottom_right

# Create buddy box
for i in range(padding_height_top, max_height - padding_height_bottom):
    OSD[i][max_width - padding_width_right] = Unicodes.vertical
    OSD[i][max_width - 1] = Unicodes.vertical

OSD[padding_height_top][max_width - padding_width_right: max_width] = [Unicodes.horizontal] * (padding_width_right)
OSD[max_height - padding_height_bottom - 1][max_width - padding_width_right: max_width] = [Unicodes.horizontal] * (
    padding_width_right)
OSD[padding_height_top][max_width - padding_width_right] = Unicodes.top_left
OSD[padding_height_top][max_width - 1] = Unicodes.top_right
OSD[max_height - padding_height_bottom - 1][max_width - padding_width_right] = Unicodes.bottom_left
OSD[max_height - padding_height_bottom - 1][max_width - 1] = Unicodes.bottom_right

# Create buddy box divisions
for i in range(0, num_uav - 1):
    OSD[padding_height_top + int((max_height - padding_height_top - padding_height_bottom) / num_uav) * (i + 1)][
    max_width - padding_width_right + 1: max_width - 1] = [Unicodes.horizontal] * (
            padding_width_right - 2)
    OSD[padding_height_top + int((max_height - padding_height_top - padding_height_bottom) / num_uav) * (i + 1)][
        max_width - padding_width_right] = Unicodes.tri_left
    OSD[padding_height_top + int((max_height - padding_height_top - padding_height_bottom) / num_uav) * (i + 1)][
        max_width - 1] = Unicodes.tri_right

# Determine OSD center and put arrow to represent current UAV
center_y = int(math.ceil(float(max_height - padding_height_top - padding_height_bottom) / 2))
center_x = int(math.ceil(float(max_width - padding_width_left - padding_width_right) / 2))
center = Point2D(center_x, center_y)
OSD[center.y][center.x] = Chlorophyll.BLINK + Unicodes.up_arrow + Chlorophyll.END

# This is my UAV
my_uav = UAV(5, 5)

# These are my UAV buddies
my_crew = [UAV() for _ in range(5)]

# Let me place my buddies in map
my_crew[0] = UAV(-5, 10, 100, True, "TKO", "SRC", 0, 5000, 1500, 750, 3.256879, 2.365646, 100, 47.456421, -2.456456,
                 500, 343.564656, 140, 50, 207, 3.4678951)
my_crew[1] = UAV(-5, 11, 101, True, "LND", "TRK", 0, 6000, 1600, 850, 4.256879, 3.365646, 200, 57.456421, 3.456456,
                 600, 243.564656, 240, 60, 307, 43.4678951)
my_crew[2] = UAV(-5, 12, 102, True, "RTL", "SRC", 0, 7000, 1700, 950, 5.256879, 4.365646, 300, 67.456421, -4.456456,
                 700, 143.564656, 340, 70, 407, 143.4678951)
my_crew[3] = UAV(-5, 13, 103, True, "INT", "TRK", 0, 8000, 1800, 1050, 6.256879, 5.365646, 400, 77.456421, 5.456456,
                 800, 43.564656, 440, 80, 507, 243.4678951)
my_crew[4] = UAV(-5, 14, 104, True, "INT", "SRC", 0, 9000, 1900, 1150, 7.256879, 6.365646, 500, 87.456421, -6.456456,
                 900, 3.564656, 540, 90, 607, 343.4678951)

# These are my targets
my_targets = [Target() for _ in range(5)]

# Let me place my targets in map
my_targets[0] = Target(5, 10)
my_targets[1] = Target(5, 11)
my_targets[2] = Target(5, 12)
my_targets[3] = Target(5, 13)
my_targets[4] = Target(5, 14)

# These are my buildings
my_buildings = [Building() for _ in range(5)]

# Let me place my buildings in map
my_buildings[0] = Building(17, 10)
my_buildings[1] = Building(17, 11)
my_buildings[2] = Building(17, 12)
my_buildings[3] = Building(17, 13)
my_buildings[4] = Building(17, 14)

# Print my crew to the OSD
for uav in my_crew:
    delta_x = uav.x - my_uav.x
    delta_y = uav.y - my_uav.y
    OSD[center.y - delta_y][center.x + delta_x] = Chlorophyll.BLINK + Unicodes.heart + Chlorophyll.END

# Print targets to the OSD
for target in my_targets:
    delta_x = target.x - my_uav.x
    delta_y = target.y - my_uav.y
    OSD[center.y - delta_y][center.x + delta_x] = Unicodes.circle + Chlorophyll.END

# Print buildings to the OSD
for building in my_buildings:
    bina = Chlorophyll.BOLD + Unicodes.square + Chlorophyll.END
    delta_x = building.x - my_uav.x
    delta_y = building.y - my_uav.y
    OSD[center.y - delta_y][center.x + delta_x] = Unicodes.square + Chlorophyll.END

# Create buddy informations
for i in range(0, num_uav):
    # name, arm, mode, duty, help
    first_line = "UAV:" + str(i + 1) + " Arm:I Mode:CCC Duty:CCC Help:" + str(i + 1)
    OSD[padding_height_top + int((max_height - padding_height_top - padding_height_bottom) / num_uav) * i + 1][
    max_width - padding_width_right + 1: max_width - padding_width_right + 1 + len(first_line)] = first_line

    # remaining, total and usage of battery, total travelled distance
    second_line = "Battery:IIII(IIII, I.I) Travel:IIIII"
    OSD[padding_height_top + int((max_height - padding_height_top - padding_height_bottom) / num_uav) * i + 2][
    max_width - padding_width_right + 1: max_width - padding_width_right + 1 + len(second_line)] = second_line

    # air speed, ground speed, home distance
    third_line = "AirSpd:II.II GndSpd:II.II Home:IIIII"
    OSD[padding_height_top + int((max_height - padding_height_top - padding_height_bottom) / num_uav) * i + 3][
    max_width - padding_width_right + 1: max_width - padding_width_right + 1 + len(third_line)] = third_line

    # roll, pitch and throttle values (current, not commands)
    forth_line = "Roll:-II.I Pitch:+II.I Throttle:IIII"
    OSD[padding_height_top + int((max_height - padding_height_top - padding_height_bottom) / num_uav) * i + 4][
    max_width - padding_width_right + 1: max_width - padding_width_right + 1 + len(forth_line)] = forth_line

    # x, y, altitude and yaw (azimuth)
    fifth_line = "X:+IIII Y:-IIII Alt:III.I Yaw:III.II"
    OSD[padding_height_top + int((max_height - padding_height_top - padding_height_bottom) / num_uav) * i + 5][
    max_width - padding_width_right + 1: max_width - padding_width_right + 1 + len(fifth_line)] = fifth_line

    # next waypoint number, total waypoint number, next waypoint distance, next waypoint yaw
    sixth_line = "NWP:IIII(IIII) Dist:IIIII Yaw:III.II"
    OSD[padding_height_top + int((max_height - padding_height_top - padding_height_bottom) / num_uav) * i + 6][
    max_width - padding_width_right + 1: max_width - padding_width_right + 1 + len(sixth_line)] = sixth_line

# This prints the OSD to screen, do not change it
print("\n".join("".join(x for x in row) for row in OSD))
