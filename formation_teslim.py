import time
from testucusu.uav import UAV


altitudes = [50, 100, 50, 100, 50, 100, 50, 100, 75]

# initialize the UAV
for i in range(9):
    my_uav = UAV(number=i, server_address="localhost")

    # wait for safety
    while not my_uav.safety_switch or not my_uav.scenario.received:
        time.sleep(0.0001)

    my_uav.heading = 270
    my_uav.altitude = altitudes[i]
    my_uav.x_speed = 20
    my_uav.armed = True
